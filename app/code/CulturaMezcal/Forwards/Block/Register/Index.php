<?php

namespace CulturaMezcal\Forwards\Block\Register;

use CulturaMezcal\Core\Model\AttributeOptions;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Index extends Template
{
    /**
     * @var AttributeOptions
     */
    private $attributeOptions;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country
     */
    private $country;
    /**
     * @var CollectionFactory
     */
    private $regionsFactory;

    /**
     * Index constructor.
     *
     * @param Context                                                                $context
     * @param AttributeOptions                                                       $attributeOptions
     * @param \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country $country
     * @param CollectionFactory                                                      $regionsFactory
     */
    public function __construct(
        Context $context,
        AttributeOptions $attributeOptions,
        \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country $country,
        CollectionFactory $regionsFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->attributeOptions = $attributeOptions;
        $this->country          = $country;
        $this->regionsFactory   = $regionsFactory;
    }

    public function getProfessionOptions()
    {
        return $this->attributeOptions->setAttributeCode('profession')->toOptionArray();
    }

    public function getCountries()
    {
        return $this->country->getAllOptions();
    }

    public function getRegions()
    {
        return $this->regionsFactory->create()->addFieldToFilter('country_id', 'ES')->load()->toOptionArray();
    }

    public function getWorkPlaces()
    {
        return $this->attributeOptions->setAttributeCode('work_place')->toOptionArray();
    }

    public function getPaymentPeriodicity()
    {
        return $this->attributeOptions->setAttributeCode('payment_periodicity')->toOptionArray();
    }

    public function getDeliveryDays($removeFirst = true)
    {
        $options = $this->attributeOptions->setAttributeCode('delivery_days')->toOptionArray('customer_address');
        if ($removeFirst) {
            array_shift($options);
        }

        return $options;
    }

    public function getDeliveryTimes($removeFirst = true)
    {
        $options = $this->attributeOptions->setAttributeCode('delivery_time')->toOptionArray('customer_address');
        if ($removeFirst) {
            array_shift($options);
        }

        return $options;
    }

    public function getDirectDebitTerm($removeFirst = true)
    {
        $options = $this->attributeOptions->setAttributeCode('direct_debit_term')->toOptionArray();
        if ($removeFirst) {
            array_shift($options);
        }

        return $options;
    }

}
