<?php

namespace CulturaMezcal\Forwards\Block\Adminhtml\Horeca\Contact\Edit;

use CulturaMezcal\Forwards\Api\Data\HorecaContactInterface as DataInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \CulturaMezcal\Forwards\Api\{{{MODEL}}}Interface
     */
    protected $model;
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param DataInterface                         $model
     * @param RequestInterface                      $request
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        DataInterface $model,
        RequestInterface $request
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->model      = $model;
        $this->request    = $request;
    }

    /**
     * Return the model Id
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->model ? $this->model->getId() : null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
