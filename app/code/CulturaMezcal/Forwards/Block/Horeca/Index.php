<?php

namespace CulturaMezcal\Forwards\Block\Horeca;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;


class Index extends Template
{

    public function __construct(
        Context $context,

        array $data = []
    ) {
        parent::__construct($context, $data);

    }

}
