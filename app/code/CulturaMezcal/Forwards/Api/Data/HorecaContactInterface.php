<?php

namespace CulturaMezcal\Forwards\Api\Data;

/**
 * @package CulturaMezcal\Forwards\Api
 */
interface HorecaContactInterface
{

    /**
     * @param array $fields []
     *
     * @return array
     */
    public function toArray(array $fields = []);


    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return HorecaContactInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getHorecaId();

    /**
     * @param int $horecaId
     *
     * @return HorecaContactInterface
     */
    public function setHorecaId($horecaId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return HorecaContactInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getJobTitle();

    /**
     * @param string $jobTitle
     *
     * @return HorecaContactInterface
     */
    public function setJobTitle($jobTitle);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param string $phone
     *
     * @return HorecaContactInterface
     */
    public function setPhone($phone);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     *
     * @return HorecaContactInterface
     */
    public function setEmail($email);
}


