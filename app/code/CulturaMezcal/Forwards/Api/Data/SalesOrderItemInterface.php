<?php

namespace CulturaMezcal\Forwards\Api\Data;

/**
 * @package CulturaMezcal\Forwards\Api
 */
interface SalesOrderItemInterface
{

    /**
     * @param array $fields []
     *
     * @return array
     */
    public function toArray(array $fields = []);


    /**
     * @return int
     */
    public function getItemId();

    /**
     * @param int $itemId
     *
     * @return SalesOrderItemInterface
     */
    public function setItemId($itemId);

    /**
     * @return int
     */
    public function getOrderId();

    /**
     * @param int $orderId
     *
     * @return SalesOrderItemInterface
     */
    public function setOrderId($orderId);

    /**
     * @return int
     */
    public function getParentItemId();

    /**
     * @param int $parentItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setParentItemId($parentItemId);

    /**
     * @return int
     */
    public function getQuoteItemId();

    /**
     * @param int $quoteItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setQuoteItemId($quoteItemId);

    /**
     * @return int
     */
    public function getStoreId();

    /**
     * @param int $storeId
     *
     * @return SalesOrderItemInterface
     */
    public function setStoreId($storeId);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     *
     * @return SalesOrderItemInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string $updatedAt
     *
     * @return SalesOrderItemInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return int
     */
    public function getProductId();

    /**
     * @param int $productId
     *
     * @return SalesOrderItemInterface
     */
    public function setProductId($productId);

    /**
     * @return string
     */
    public function getProductType();

    /**
     * @param string $productType
     *
     * @return SalesOrderItemInterface
     */
    public function setProductType($productType);

    /**
     * @return string
     */
    public function getProductOptions();

    /**
     * @param string $productOptions
     *
     * @return SalesOrderItemInterface
     */
    public function setProductOptions($productOptions);

    /**
     * @return float
     */
    public function getWeight();

    /**
     * @param float $weight
     *
     * @return SalesOrderItemInterface
     */
    public function setWeight($weight);

    /**
     * @return int
     */
    public function getIsVirtual();

    /**
     * @param int $isVirtual
     *
     * @return SalesOrderItemInterface
     */
    public function setIsVirtual($isVirtual);

    /**
     * @return string
     */
    public function getSku();

    /**
     * @param string $sku
     *
     * @return SalesOrderItemInterface
     */
    public function setSku($sku);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return SalesOrderItemInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     *
     * @return SalesOrderItemInterface
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getAppliedRuleIds();

    /**
     * @param string $appliedRuleIds
     *
     * @return SalesOrderItemInterface
     */
    public function setAppliedRuleIds($appliedRuleIds);

    /**
     * @return string
     */
    public function getAdditionalData();

    /**
     * @param string $additionalData
     *
     * @return SalesOrderItemInterface
     */
    public function setAdditionalData($additionalData);

    /**
     * @return int
     */
    public function getIsQtyDecimal();

    /**
     * @param int $isQtyDecimal
     *
     * @return SalesOrderItemInterface
     */
    public function setIsQtyDecimal($isQtyDecimal);

    /**
     * @return int
     */
    public function getNoDiscount();

    /**
     * @param int $noDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setNoDiscount($noDiscount);

    /**
     * @return float
     */
    public function getQtyBackordered();

    /**
     * @param float $qtyBackordered
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyBackordered($qtyBackordered);

    /**
     * @return float
     */
    public function getQtyCanceled();

    /**
     * @param float $qtyCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyCanceled($qtyCanceled);

    /**
     * @return float
     */
    public function getQtyInvoiced();

    /**
     * @param float $qtyInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyInvoiced($qtyInvoiced);

    /**
     * @return float
     */
    public function getQtyOrdered();

    /**
     * @param float $qtyOrdered
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyOrdered($qtyOrdered);

    /**
     * @return float
     */
    public function getQtyRefunded();

    /**
     * @param float $qtyRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyRefunded($qtyRefunded);

    /**
     * @return float
     */
    public function getQtyShipped();

    /**
     * @param float $qtyShipped
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyShipped($qtyShipped);

    /**
     * @return float
     */
    public function getBaseCost();

    /**
     * @param float $baseCost
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseCost($baseCost);

    /**
     * @return float
     */
    public function getPrice();

    /**
     * @param float $price
     *
     * @return SalesOrderItemInterface
     */
    public function setPrice($price);

    /**
     * @return float
     */
    public function getBasePrice();

    /**
     * @param float $basePrice
     *
     * @return SalesOrderItemInterface
     */
    public function setBasePrice($basePrice);

    /**
     * @return float
     */
    public function getOriginalPrice();

    /**
     * @param float $originalPrice
     *
     * @return SalesOrderItemInterface
     */
    public function setOriginalPrice($originalPrice);

    /**
     * @return float
     */
    public function getBaseOriginalPrice();

    /**
     * @param float $baseOriginalPrice
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseOriginalPrice($baseOriginalPrice);

    /**
     * @return float
     */
    public function getTaxPercent();

    /**
     * @param float $taxPercent
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxPercent($taxPercent);

    /**
     * @return float
     */
    public function getTaxAmount();

    /**
     * @param float $taxAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxAmount($taxAmount);

    /**
     * @return float
     */
    public function getBaseTaxAmount();

    /**
     * @param float $baseTaxAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxAmount($baseTaxAmount);

    /**
     * @return float
     */
    public function getTaxInvoiced();

    /**
     * @param float $taxInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxInvoiced($taxInvoiced);

    /**
     * @return float
     */
    public function getBaseTaxInvoiced();

    /**
     * @param float $baseTaxInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxInvoiced($baseTaxInvoiced);

    /**
     * @return float
     */
    public function getDiscountPercent();

    /**
     * @param float $discountPercent
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountPercent($discountPercent);

    /**
     * @return float
     */
    public function getDiscountAmount();

    /**
     * @param float $discountAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountAmount($discountAmount);

    /**
     * @return float
     */
    public function getBaseDiscountAmount();

    /**
     * @param float $baseDiscountAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountAmount($baseDiscountAmount);

    /**
     * @return float
     */
    public function getDiscountInvoiced();

    /**
     * @param float $discountInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountInvoiced($discountInvoiced);

    /**
     * @return float
     */
    public function getBaseDiscountInvoiced();

    /**
     * @param float $baseDiscountInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountInvoiced($baseDiscountInvoiced);

    /**
     * @return float
     */
    public function getAmountRefunded();

    /**
     * @param float $amountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setAmountRefunded($amountRefunded);

    /**
     * @return float
     */
    public function getBaseAmountRefunded();

    /**
     * @param float $baseAmountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseAmountRefunded($baseAmountRefunded);

    /**
     * @return float
     */
    public function getRowTotal();

    /**
     * @param float $rowTotal
     *
     * @return SalesOrderItemInterface
     */
    public function setRowTotal($rowTotal);

    /**
     * @return float
     */
    public function getBaseRowTotal();

    /**
     * @param float $baseRowTotal
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowTotal($baseRowTotal);

    /**
     * @return float
     */
    public function getRowInvoiced();

    /**
     * @param float $rowInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setRowInvoiced($rowInvoiced);

    /**
     * @return float
     */
    public function getBaseRowInvoiced();

    /**
     * @param float $baseRowInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowInvoiced($baseRowInvoiced);

    /**
     * @return float
     */
    public function getRowWeight();

    /**
     * @param float $rowWeight
     *
     * @return SalesOrderItemInterface
     */
    public function setRowWeight($rowWeight);

    /**
     * @return float
     */
    public function getBaseTaxBeforeDiscount();

    /**
     * @param float $baseTaxBeforeDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxBeforeDiscount($baseTaxBeforeDiscount);

    /**
     * @return float
     */
    public function getTaxBeforeDiscount();

    /**
     * @param float $taxBeforeDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxBeforeDiscount($taxBeforeDiscount);

    /**
     * @return string
     */
    public function getExtOrderItemId();

    /**
     * @param string $extOrderItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setExtOrderItemId($extOrderItemId);

    /**
     * @return int
     */
    public function getLockedDoInvoice();

    /**
     * @param int $lockedDoInvoice
     *
     * @return SalesOrderItemInterface
     */
    public function setLockedDoInvoice($lockedDoInvoice);

    /**
     * @return int
     */
    public function getLockedDoShip();

    /**
     * @param int $lockedDoShip
     *
     * @return SalesOrderItemInterface
     */
    public function setLockedDoShip($lockedDoShip);

    /**
     * @return float
     */
    public function getPriceInclTax();

    /**
     * @param float $priceInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setPriceInclTax($priceInclTax);

    /**
     * @return float
     */
    public function getBasePriceInclTax();

    /**
     * @param float $basePriceInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setBasePriceInclTax($basePriceInclTax);

    /**
     * @return float
     */
    public function getRowTotalInclTax();

    /**
     * @param float $rowTotalInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setRowTotalInclTax($rowTotalInclTax);

    /**
     * @return float
     */
    public function getBaseRowTotalInclTax();

    /**
     * @param float $baseRowTotalInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowTotalInclTax($baseRowTotalInclTax);

    /**
     * @return float
     */
    public function getDiscountTaxCompensationAmount();

    /**
     * @param float $discountTaxCompensationAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationAmount($discountTaxCompensationAmount);

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationAmount();

    /**
     * @param float $baseDiscountTaxCompensationAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationAmount($baseDiscountTaxCompensationAmount);

    /**
     * @return float
     */
    public function getDiscountTaxCompensationInvoiced();

    /**
     * @param float $discountTaxCompensationInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationInvoiced($discountTaxCompensationInvoiced);

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationInvoiced();

    /**
     * @param float $baseDiscountTaxCompensationInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationInvoiced($baseDiscountTaxCompensationInvoiced);

    /**
     * @return float
     */
    public function getDiscountTaxCompensationRefunded();

    /**
     * @param float $discountTaxCompensationRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationRefunded($discountTaxCompensationRefunded);

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationRefunded();

    /**
     * @param float $baseDiscountTaxCompensationRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationRefunded($baseDiscountTaxCompensationRefunded);

    /**
     * @return float
     */
    public function getTaxCanceled();

    /**
     * @param float $taxCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxCanceled($taxCanceled);

    /**
     * @return float
     */
    public function getDiscountTaxCompensationCanceled();

    /**
     * @param float $discountTaxCompensationCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationCanceled($discountTaxCompensationCanceled);

    /**
     * @return float
     */
    public function getTaxRefunded();

    /**
     * @param float $taxRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxRefunded($taxRefunded);

    /**
     * @return float
     */
    public function getBaseTaxRefunded();

    /**
     * @param float $baseTaxRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxRefunded($baseTaxRefunded);

    /**
     * @return float
     */
    public function getDiscountRefunded();

    /**
     * @param float $discountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountRefunded($discountRefunded);

    /**
     * @return float
     */
    public function getBaseDiscountRefunded();

    /**
     * @param float $baseDiscountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountRefunded($baseDiscountRefunded);

    /**
     * @return int
     */
    public function getFreeShipping();

    /**
     * @param int $freeShipping
     *
     * @return SalesOrderItemInterface
     */
    public function setFreeShipping($freeShipping);

    /**
     * @return int
     */
    public function getGiftMessageId();

    /**
     * @param int $giftMessageId
     *
     * @return SalesOrderItemInterface
     */
    public function setGiftMessageId($giftMessageId);

    /**
     * @return int
     */
    public function getGiftMessageAvailable();

    /**
     * @param int $giftMessageAvailable
     *
     * @return SalesOrderItemInterface
     */
    public function setGiftMessageAvailable($giftMessageAvailable);

    /**
     * @return string
     */
    public function getWeeeTaxApplied();

    /**
     * @param string $weeeTaxApplied
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxApplied($weeeTaxApplied);

    /**
     * @return float
     */
    public function getWeeeTaxAppliedAmount();

    /**
     * @param float $weeeTaxAppliedAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxAppliedAmount($weeeTaxAppliedAmount);

    /**
     * @return float
     */
    public function getWeeeTaxAppliedRowAmount();

    /**
     * @param float $weeeTaxAppliedRowAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxAppliedRowAmount($weeeTaxAppliedRowAmount);

    /**
     * @return float
     */
    public function getWeeeTaxDisposition();

    /**
     * @param float $weeeTaxDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxDisposition($weeeTaxDisposition);

    /**
     * @return float
     */
    public function getWeeeTaxRowDisposition();

    /**
     * @param float $weeeTaxRowDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxRowDisposition($weeeTaxRowDisposition);

    /**
     * @return float
     */
    public function getBaseWeeeTaxAppliedAmount();

    /**
     * @param float $baseWeeeTaxAppliedAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxAppliedAmount($baseWeeeTaxAppliedAmount);

    /**
     * @return float
     */
    public function getBaseWeeeTaxAppliedRowAmnt();

    /**
     * @param float $baseWeeeTaxAppliedRowAmnt
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxAppliedRowAmnt($baseWeeeTaxAppliedRowAmnt);

    /**
     * @return float
     */
    public function getBaseWeeeTaxDisposition();

    /**
     * @param float $baseWeeeTaxDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxDisposition($baseWeeeTaxDisposition);

    /**
     * @return float
     */
    public function getBaseWeeeTaxRowDisposition();

    /**
     * @param float $baseWeeeTaxRowDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxRowDisposition($baseWeeeTaxRowDisposition);
}


