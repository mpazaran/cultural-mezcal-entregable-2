<?php

namespace CulturaMezcal\Forwards\Api\Data;

/**
 * @package CulturaMezcal\Forwards\Api
 */
interface DirectoryCountryRegionTownInterface
{

    /**
     * @param array $fields []
     *
     * @return array
     */
    public function toArray(array $fields = []);


    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getRegionId();

    /**
     * @param int $regionId
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setRegionId($regionId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return DirectoryCountryRegionTownInterface
     */
    public function setName($name);
}


