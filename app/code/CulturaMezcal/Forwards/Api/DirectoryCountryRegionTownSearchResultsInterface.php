<?php

namespace CulturaMezcal\Forwards\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface DirectoryCountryRegionTownSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \CulturaMezcal\Forwards\Api\Data\DirectoryCountryRegionTownInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
