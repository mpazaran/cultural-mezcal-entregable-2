<?php

namespace CulturaMezcal\Forwards\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface HorecaContactSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \CulturaMezcal\Forwards\Api\Data\HorecaContactInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
