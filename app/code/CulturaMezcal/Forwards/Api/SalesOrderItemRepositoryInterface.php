<?php

namespace CulturaMezcal\Forwards\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @package CulturaMezcal\Forwards\Api
 */
interface SalesOrderItemRepositoryInterface extends OptionSourceInterface
{

    /**
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     */
    public function create();

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \CulturaMezcal\Forwards\Api\SalesOrderItemSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @return \CulturaMezcal\Forwards\Model\ResourceModel\SalesOrderItem\Collection
     */
    public function getCollection();

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model, $value, $field = null);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     */
    public function save(\CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model);

    /**
     * @param \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     */
    public function delete(\CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface $model);

    /**
     * @param int $id
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null);

}
