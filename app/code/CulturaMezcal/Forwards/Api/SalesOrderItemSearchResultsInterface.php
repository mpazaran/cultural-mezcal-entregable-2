<?php

namespace CulturaMezcal\Forwards\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface SalesOrderItemSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \CulturaMezcal\Forwards\Api\Data\SalesOrderItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
