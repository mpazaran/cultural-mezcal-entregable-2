<?php /** @noinspection ALL */

namespace CulturaMezcal\Forwards\Setup;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Zend_Validate_Exception;


/**
 * Magento goes through your module’s data initialization stages after the schema initialization processes complete.
 *
 * Magento executes the data installation class during your module’s initial install unless an existing version entry is found in the database.
 * The purpose of this class is to populate the database with initial data.
 *
 * @author {{author}}
 * @see    https://devdocs.magento.com/guides/v2.0/extension-dev-guide/prepare/lifecycle.html
 */
class InstallData implements InstallDataInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;
    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * PatchInitial constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory          $eavSetupFactory
     * @param SalesSetupFactory        $salesSetupFactory
     * @param Config                   $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        Config $eavConfig
    ) {
        $this->moduleDataSetup   = $moduleDataSetup;
        $this->eavSetupFactory   = $eavSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->eavConfig         = $eavConfig;
    }


    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     *
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     * @throws \Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $textAttributes = [
            'forward_id'    => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Forward',
                'note'        => 'Only Horeca'
            ],
            'mothers_name'    => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Mothers name',
                'note'        => 'Only Forward'
            ],
            'phone'          => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Phone',
                'note'        => 'Forward and Horeca'
            ],
            'personal_street' => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Street',
                'note'        => 'Only Forward'
            ],
            'internal_number' => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Internal Number',
                'note'        => 'Only Forward'
            ],
            'external_number' => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'External Number',
                'note'        => 'Only Forward'
            ],
            'zip_code'        => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Zip Code',
                'note'        => 'Only Forward'
            ],
            'cif'             => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'CIF',
                'note'        => 'Forward and Horeca',
                'use_in_grid' => true,
            ],
            'bank_account'    => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Bank Account',
                'note'        => 'Only Forward',
                'use_in_grid' => true,
            ],
            'web'             => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Web',
                'note'        => 'Only Horeca'
            ],
            'iban'            => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'IBAN',
                'note'        => 'Only Horeca',
                'use_in_grid' => true,
            ],
            'notes'           => [
                'entity_type' => 'customer_address',
                'label'       => 'Notes',
            ],
            'external_number' => [
                'entity_type' => 'customer_address',
                'label'       => 'External Number',
            ],
            'internal_number' => [
                'entity_type' => 'customer_address',
                'label'       => 'Internal Number',
            ],
        ];

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $setup->getConnection()->disallowDdlCache();

        $position = 1000;
        foreach ($textAttributes as $attributeCode => $definition) {

            $eavSetup->addAttribute(
                $definition['entity_type'],
                $attributeCode,
                [
                    'type'         => 'varchar',
                    'label'        => $definition['label'],
                    'input'        => 'text',
                    'required'     => false,
                    'visible'      => true,
                    'user_defined' => false,
                    'position'     => $position,
                    'system'       => 0,
                    'group'        => 'General',
                    'note'         => $definition['note'] ?? null
                ]
            );

            $position += 10;

            $attribute = $this->eavConfig->getAttribute($definition['entity_type'], $attributeCode);
            $attribute->setData(
                'used_in_forms',
                $definition['entity_type'] === Customer::ENTITY
                    ?
                    ['adminhtml_customer', 'customer_account_edit']
                    :
                    ['adminhtml_customer_address', 'customer_address_edit']
            );

            if (!empty($definition['use_in_grid'])) {
                $attribute->setData('is_used_in_grid', 1);
                $attribute->setData('is_visible_in_grid', 1);
                $attribute->setData('is_filterable_in_grid', 1);
            }

            $attribute->save();
        }

        $selectAttributes = [
            'work_place'          => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Work Place',
                'note'        => 'Only Forward',
                'option'      => [
                    'values' => [
                        'BAR',
                        'OFFICE',
                        'STAFF'
                    ]
                ],
                'multiple'    => false,
                'use_in_grid' => true,
            ],
            'payment_periodicity' => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Payment Periodicity',
                'option'      => [
                    'values' => [
                        'weekly',
                        'biweekly',
                        'monthly'
                    ]
                ],
                'multiple'    => false,
                'note'        => 'Only Forward',
                'use_in_grid' => true,
            ],
            'direct_debit_term'   => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Direct Debit Payment Term',
                'option'      => [
                    'values' => [
                        '15 Days',
                        '1 Month',
                        '2 Month',
                        '3 Month',
                    ]
                ],
                'multiple'    => false,
                'note'        => 'Only Horeca',
                'use_in_grid' => true,
            ],
            'forward_status'      => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Forward Status',
                'option'      => [
                    'values' => [
                        'Pending',
                        'Approved',
                        'Declined',
                        'Inactive',
                    ]
                ],
                'multiple'    => false,
                'note'        => 'Only Forward',
                'use_in_grid' => true,
            ],
            'horeca_status'       => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'HORECA Status',
                'option'      => [
                    'values' => [
                        'Pending',
                        'Registered by Forward',
                        'Approved',
                        'Declined',
                        'Inactive',
                    ]
                ],
                'multiple'    => false,
                'note'        => 'Only Horeca',
                'use_in_grid' => true,
            ],
            'profession'          => [
                'entity_type' => Customer::ENTITY,
                'label'       => 'Profession',
                'option'      => [
                    'values' => [
                        'BARTENDER',
                        'SALES AGENT',
                        'OWNER',
                    ]
                ],
                'multiple'    => false,
                'note'        => 'Only Forward',
                'use_in_grid' => true,
            ],
            'country_id'          => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'Country',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Region',
            ],
            'region_id'           => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'Region',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Region',
            ],
            'town_id'             => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'City',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'CulturaMezcal\Forwards\Model\ResourceModel\Town',
            ],
            'sales_country_id'    => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'Sales Country',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country',
            ],
            'sales_region_id'     => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'Sales Region',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Region',
            ],
            'sales_town_id'       => [
                'entity_type'  => Customer::ENTITY,
                'label'        => 'Sales City',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'CulturaMezcal\Forwards\Model\ResourceModel\Town',
            ],
            'delivery_days'       => [
                'entity_type' => 'customer_address',
                'label'       => 'Delivery Days',
                'option'      => [
                    'values' => [
                        'Monday',
                        'Tuesday',
                        'Wednesday',
                        'Thursday',
                        'Friday'
                    ]
                ],
                'multiple'    => true,
                'note'        => 'Only Horeca',
                'use_in_grid' => true,
            ],
            'delivery_time'       => [
                'entity_type' => 'customer_address',
                'label'       => 'Delivery Schedule',
                'option'      => [
                    'values' => [
                        'Morning',
                        'Noon',
                        'Afternoon',
                        'Night',
                    ]
                ],
                'multiple'    => true,
                'note'        => 'Only Horeca',
                'use_in_grid' => true,
            ],
            'town_id'             => [
                'entity_type' => 'customer_address',
                'label'        => 'City',
                'multiple'     => false,
                'note'         => 'Only Forward',
                'source_model' => 'CulturaMezcal\Forwards\Model\ResourceModel\Town',
            ],
        ];

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        foreach ($selectAttributes as $attributeCode => $definition) {

            $attributeData = [
                'type'          => $definition['multiple'] || strpos($attributeCode, 'country_id') !== false ? 'varchar' : 'int',
                'label'         => $definition['label'],
                'input'         => $definition['multiple'] ? 'multiselect' : 'select',
                'multiple'      => $definition['multiple'],
                'required'      => false,
                'visible'       => true,
                'user_defined'  => false,
                'default_value' => $definition['multiple'] ? '' : 0,
                'position'      => $position,
                'system'        => 0,
                'group'         => 'General',
                'source_model'  => Table::class,
                'note'          => $definition['note']
            ];

            if (isset($definition['option'])) {
                $attributeData['option'] = $definition['option'];
            }

            if (isset($definition['source_model'])) {
                $attributeData['source_model'] = $definition['source_model'];
            }

            $eavSetup->addAttribute($definition['entity_type'], $attributeCode, $attributeData);

            $position += 10;

            $attribute = $this->eavConfig->getAttribute($definition['entity_type'], $attributeCode);

            $attribute->setData(
                'used_in_forms',
                $definition['entity_type'] === Customer::ENTITY
                    ?
                    ['adminhtml_customer', 'customer_account_edit']
                    :
                    ['adminhtml_customer_address', 'customer_address_edit']
            );

            if (!empty($definition['use_in_grid'])) {
                $attribute->setData('is_used_in_grid', 1);
                $attribute->setData('is_visible_in_grid', 1);
                $attribute->setData('is_filterable_in_grid', 1);
            }

            $attribute->setSourceModel(empty($definition['source_model']) ? Table::class : $definition['source_model']);
            if (empty($definition['source_model'])) {
                $attribute->setBackendModel(ArrayBackend::class);
            }

            $attribute->save();
        }

        $connection = $setup->getConnection();
        $townTable  = $setup->getTable('directory_country_region_town');
        $regions    = [
            131 => [
                'Alegría-Dulantzi',
                'Amurrio',
                'Añana',
                'Aramaio',
                'Armiñón',
                'Arraia-Maeztu',
                'Arrazua-Ubarrundia',
                'Artziniega',
                'Asparrena',
                'Ayala/Aiara',
                'Baños de Ebro/Mañueta',
                'Barrundia',
                'Berantevilla',
                'Bernedo',
                'Campezo/Kanpezu',
                'Elburgo/Burgelu',
                'Elciego',
                'Elvillar/Bilar',
                'Erriberagoitia/Ribera Alta',
                'Harana/Valle de Arana',
                'Iruña Oka/Iruña de Oca',
                'Iruraiz-Gauna',
                'Kripan',
                'Kuartango',
                'Labastida/Bastida',
                'Lagrán',
                'Laguardia',
                'Lanciego/Lantziego',
                'Lantarón',
                'Lapuebla de Labarca',
                'Laudio/Llodio',
                'Legutio',
                'Leza',
                'Moreda de Álava',
                'Navaridas',
                'Okondo',
                'Oyón-Oion',
                'Peñacerrada-Urizaharra',
                'Ribera Baja/Erribera Beitia',
                'Salvatierra/Agurain',
                'Samaniego',
                'San Millán/Donemiliaga',
                'Urkabustaiz',
                'Valdegovía/Gaubea',
                'Villabuena de Álava/Eskuernaga',
                'Vitoria-Gasteiz',
                'Yécora/Iekora',
                'Zalduondo',
                'Zambrana',
                'Zigoitia',
                'Zuia'
            ],
            132 => [
                'Abengibre',
                'Alatoz',
                'Albacete',
                'Albatana',
                'Alborea',
                'Alcadozo',
                'Alcalá del Júcar',
                'Alcaraz',
                'Almansa',
                'Alpera',
                'Ayna',
                'Balazote',
                'Ballestero, El',
                'Balsa de Ves',
                'Barrax',
                'Bienservida',
                'Bogarra',
                'Bonete',
                'Bonillo, El',
                'Carcelén',
                'Casas de Juan Núñez',
                'Casas de Lázaro',
                'Casas de Ves',
                'Casas-Ibáñez',
                'Caudete',
                'Cenizate',
                'Chinchilla de Monte-Aragón',
                'Corral-Rubio',
                'Cotillas',
                'Elche de la Sierra',
                'Férez',
                'Fuensanta',
                'Fuente-Álamo',
                'Fuentealbilla',
                'Gineta, La',
                'Golosalvo',
                'Hellín',
                'Herrera, La',
                'Higueruela',
                'Hoya-Gonzalo',
                'Jorquera',
                'Letur',
                'Lezuza',
                'Liétor',
                'Madrigueras',
                'Mahora',
                'Masegoso',
                'Minaya',
                'Molinicos',
                'Montalvos',
                'Montealegre del Castillo',
                'Motilleja',
                'Munera',
                'Navas de Jorquera',
                'Nerpio',
                'Ontur',
                'Ossa de Montiel',
                'Paterna del Madera',
                'Peñas de San Pedro',
                'Peñascosa',
                'Pétrola',
                'Povedilla',
                'Pozo Cañada',
                'Pozohondo',
                'Pozo-Lorente',
                'Pozuelo',
                'Recueja, La',
                'Riópar',
                'Robledo',
                'Roda, La',
                'Salobre',
                'San Pedro',
                'Socovos',
                'Tarazona de la Mancha',
                'Tobarra',
                'Valdeganga',
                'Vianos',
                'Villa de Ves',
                'Villalgordo del Júcar',
                'Villamalea',
                'Villapalacios',
                'Villarrobledo',
                'Villatoya',
                'Villavaliente',
                'Villaverde de Guadalimar',
                'Viveros',
                'Yeste',
            ]
        ];
        foreach ($regions as $regionId => $towns) {
            foreach ($towns as $town) {
                $connection->insert($townTable, [
                    'region_id' => $regionId,
                    'name'      => $town
                ]);
            }
        }

        $setup->endSetup();
    }
}
