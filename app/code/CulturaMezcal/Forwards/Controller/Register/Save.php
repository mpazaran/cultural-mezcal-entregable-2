<?php

namespace CulturaMezcal\Forwards\Controller\Register;

use CulturaMezcal\Core\Helper\Data;
use CulturaMezcal\Forwards\Logger\Logger;
use Exception;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface as ModelRepository;
use Magento\Customer\Api\Data\CustomerInterfaceFactory as CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

class Save extends Action
{

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var SerializerJson $serializerJson
     */
    protected $serializerJson;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var Context
     */
    private $context;
    /**
     * @var ModelRepository
     */
    private $modelRepository;
    /**
     * @var CustomerFactory
     */
    private $customerFactory;
    /**
     * @var Data
     */
    private $culturaMezcal;
    /**
     * @var Encryptor
     */
    private $encryptor;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var AccountManagementInterface
     */
    private $customerAccountManagement;

    /**
     * Search constructor.
     *
     * @param Context                    $context
     * @param Session                    $customerSession
     * @param SerializerJson             $serializerJson
     * @param JsonFactory                $jsonResultFactory
     * @param ModelRepository            $modelRepository
     * @param CustomerFactory            $customerFactory
     * @param Data                       $culturaMezcal
     * @param Encryptor                  $encryptor
     * @param AccountManagementInterface $customerAccountManagement
     * @param Logger                     $logger
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        SerializerJson $serializerJson,
        JsonFactory $jsonResultFactory,
        ModelRepository $modelRepository,
        CustomerFactory $customerFactory,
        Data $culturaMezcal,
        Encryptor $encryptor,
        AccountManagementInterface $customerAccountManagement,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->customerSession           = $customerSession;
        $this->serializerJson            = $serializerJson;
        $this->messageManager            = $context->getMessageManager();
        $this->logger                    = $logger;
        $this->context                   = $context;
        $this->modelRepository           = $modelRepository;
        $this->customerFactory           = $customerFactory;
        $this->culturaMezcal             = $culturaMezcal;
        $this->encryptor                 = $encryptor;
        $this->jsonResultFactory         = $jsonResultFactory;
        $this->customerAccountManagement = $customerAccountManagement;
    }

    /**
     * Execute action based on request and return result
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->jsonResultFactory->create();

        /**
         * If the customer is not logged in the service return an empty response
         */
        if ($this->customerSession->isLoggedIn()) {
            $result->setData([
                'redirect' => '/customer/account'
            ]);

            return $result;
        }

        $data = $this->serializerJson->unserialize($this->getRequest()->getContent());

        try {
            $this->modelRepository->get($data['email']);

            $result->setData(['ok' => false]);
            $this->messageManager->addWarningMessage(__('Already exists an account with that email.'));
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerFactory->create();
            $customer
                ->setEmail($data['email'])
                ->setDob($data['dob'])
                ->setFirstname($data['firstname'])
                ->setLastname($data['lastname'])
                ->setGroupId($this->culturaMezcal->getForwardCustomerGroupId())
                ->setStoreId($this->culturaMezcal->getCurrentStoreId())
                ->setWebsiteId($this->culturaMezcal->getCurrentWebsiteId());

            $extraAttributes = [
                'mothersname',
                'phone',
                'address_street',
                'external_number',
                'internal_number',
                'zip_code',
                'region_id',
                'town_id',
                'cif',
                'bank_account',
                'profession',
                'job_country_id',
                'job_region_id',
                'job_town_id',
                'work_place',
                'payment_periodicity',
            ];
            foreach ($extraAttributes as $extraAttribute) {
                $customer->setCustomAttribute($extraAttribute, $data[$extraAttribute] ?? null);
            }

            $customer->setCustomAttribute(
                'forward_status',
                $this->culturaMezcal->getAttribtueOptionValueByLabel('forward_status', 'Pending')
            );

            $this->modelRepository->save($customer, $this->encryptor->getHash($data['password'], true));

            $this->customerSession->loginById($customer->getId());

            $result->setHeader('Login-Required', 'true');
            $result->setData(
                [
                    'ok'       => true,
                    'redirect' => $this->_url->getUrl('customer/account/login')
                ]
            );
            $this->messageManager->addSuccessMessage(__('Congratulations your account has been created, an administrator will validate your information before you can use your account.'));
            /*$customerAuthenticated = $this->customerAccountManagement->authenticate($data['email'], $data['password']);
            $this->customerSession->setCustomerDataAsLoggedIn($customerAuthenticated);*/
        } catch (Exception $e) {
            $result->setData(['ok' => false, 'exception' => $e->getMessage()]);
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong saving the forward.'));
        }

        return $result;
    }
}
