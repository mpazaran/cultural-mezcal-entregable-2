<?php

namespace CulturaMezcal\Forwards\Controller\Horeca;

use Magento\Customer\Api\CustomerRepositoryInterface as RepositoryInterface;
use Magento\Customer\Model\CustomerSearchResults as ModelList;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;


class Search extends Action
{

    /**
     * @var RepositoryInterface
     */
    private $modelRepository;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * Search constructor.
     *
     * @param Context               $context
     * @param CustomerSession       $customerSession
     * @param JsonFactory           $resultFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder      $sortOrderBuilder
     * @param FilterBuilder         $filterBuilder
     * @param FilterGroupBuilder    $filterGroupBuilder
     * @param RepositoryInterface   $modelRepository
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        JsonFactory $resultFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        RepositoryInterface $modelRepository
    ) {
        parent::__construct($context);
        $this->resultFactory         = $resultFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->modelRepository       = $modelRepository;
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroupBuilder    = $filterGroupBuilder;
        $this->customerSession       = $customerSession;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->resultFactory->create();

        /**
         * If the customer is not logged in the service return an empty response
         */
        if (!$this->customerSession->isLoggedIn()) {
            $result->setData([
                'redirect' => '/customer/account/login'
            ]);

            return $result;
        }

        $filter         = $this->getRequest()->getParam('f');
        $order          = $this->getRequest()->getParam('o');
        $orderDirection = $this->getRequest()->getParam('d');
        $currentPage    = $this->getRequest()->getParam('p');
        $pageSize       = $this->getRequest()->getParam('ps');

        if ($pageSize != 20 && $pageSize != 50) {
            /**
             * To avoid performance lags, the limit is 50 and default is 20
             */
            $pageSize = 20;
        }


        $filterGroups = [
            $this->filterGroupBuilder->addFilter(
                $this->filterBuilder->setField('forward_id')
                    ->setValue($this->customerSession->getId())
                    ->create()
            )->create()
        ];

        if (!empty($filter)) {
            /**
             * Made the search using an OR instead of an AND is more usable
             */
            $this->filterGroupBuilder->addFilter(
                $this->filterBuilder->setField('email')
                    ->setValue('%' . $filter . '%')
                    ->setConditionType('like')
                    ->create()
            )->addFilter(
                $this->filterBuilder->setField('firstname')
                    ->setValue('%' . $filter . '%')
                    ->setConditionType('like')
                    ->create()
            )->addFilter(
                $this->filterBuilder->setField('lastname')
                    ->setValue('%' . $filter . '%')
                    ->setConditionType('like')
                    ->create()
            );

            $filterGroups[] = $this->filterGroupBuilder->create();
        }
        $this->searchCriteriaBuilder->setFilterGroups($filterGroups);

        if (!empty($order)) {
            $sort = $this->sortOrderBuilder->create();
            $sort->setDirection($orderDirection == 'asc' ? 'ASC' : 'DESC')->setField($order);
            $this->searchCriteriaBuilder->addSortOrder($sort);
        }

        $this->searchCriteriaBuilder->setCurrentPage($currentPage);
        $this->searchCriteriaBuilder->setPageSize($pageSize);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        /**
         * @var $list ModelList
         */
        $list     = $this->modelRepository->getList($searchCriteria);
        $models   = $list->getItems();
        $response = [
            'models' => [],
            'count'  => $list->getTotalCount(),//Don't use count
        ];

        foreach ($models as $model) {
            $response['models'][] = [
                'id'        => $model->getId(),
                'email'     => $model->getEmail(),
                'firstname' => $model->getFirstname(),
                'lastname'  => $model->getLastname(),
            ];
        }

        $result->setData($response);

        return $result;

    }

}
