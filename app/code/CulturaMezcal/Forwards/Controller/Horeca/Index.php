<?php

namespace CulturaMezcal\Forwards\Controller\Horeca;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        CustomerSession $customerSession
    ) {
        parent::__construct($context);
        $this->pageFactory     = $pageFactory;
        $this->customerSession = $customerSession;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        if ($this->customerSession->isLoggedIn()) {
            return $this->pageFactory->create();
        } else {
            /**
             * @var $redirectResult Redirect
             */
            $redirectResult = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirectResult->setPath('customer/account/login');
            return $redirectResult;
        }
    }
}
