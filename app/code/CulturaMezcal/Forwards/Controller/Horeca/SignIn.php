<?php

namespace CulturaMezcal\Forwards\Controller\Horeca;

use CulturaMezcal\Core\Helper\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

class SignIn extends Action
{

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var Data
     */
    private $culturaMezcal;
    /**
     * @var Context
     */
    private $context;
    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * Index constructor.
     *
     * @param Context                     $context
     * @param PageFactory                 $pageFactory
     * @param CustomerSession             $customerSession
     * @param CheckoutSession             $checkoutSession
     * @param Data                        $culturaMezcal
     * @param AccountManagementInterface  $accountManagement
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        Data $culturaMezcal,
        AccountManagementInterface $accountManagement,
        CustomerRepositoryInterface $customerRepository
    ) {
        parent::__construct($context);
        $this->pageFactory        = $pageFactory;
        $this->customerSession    = $customerSession;
        $this->culturaMezcal      = $culturaMezcal;
        $this->context            = $context;
        $this->accountManagement  = $accountManagement;
        $this->customerRepository = $customerRepository;
        $this->checkoutSession    = $checkoutSession;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        /**
         * @var $redirectResult Redirect
         */
        $redirectResult = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirectResult->setPath('customer/account');
        if ($this->customerSession->isLoggedIn()) {
            if ($this->culturaMezcal->isForward()) {
                $customer = $this->customerRepository->get(base64_decode($this->_request->getParam('e')));
                if ($customer->getId() == base64_decode($this->_request->getParam('i'))) {
                    if ($customer->getCustomAttribute('forward_id')->getValue() == $this->customerSession->getId()) {
                        $forwardId   = $this->customerSession->getId();
                        $forwardName = $this->customerSession->getCustomer()->getName();
                        $this->customerSession->logout();
                        $this->customerSession->loginById($customer->getId());
                        $this->customerSession->setForwardId($forwardId);
                        $this->customerSession->setForwardName($forwardName);
                        $this->checkoutSession->setIsForwardPresent(true);
                    }
                }
            }
        } else {
            $redirectResult->setPath('customer/account/login');
        }

        return $redirectResult;
    }
}
