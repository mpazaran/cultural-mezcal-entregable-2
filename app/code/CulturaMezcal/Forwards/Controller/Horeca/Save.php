<?php

namespace CulturaMezcal\Forwards\Controller\Horeca;

use CulturaMezcal\Core\Helper\Data;
use CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface;
use CulturaMezcal\Forwards\Api\HorecaContactRepositoryInterface;
use CulturaMezcal\Forwards\Logger\Logger;
use Exception;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface as RepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory as CustomerFactory;
use Magento\Customer\Model\CustomerFactory as ModelFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

/**
 * @package \CulturaMezcal\Forwards\Controller\Horeca
 */
class Save extends Action
{

    /**
     * @var RepositoryInterface
     */
    private $modelRepository;

    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var SerializerJson $serializerJson
     */
    protected $serializerJson;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var MessageManager
     */
    protected $messageManager;
    /**
     * @var Data
     */
    private $culturaMezcal;
    /**
     * @var Encryptor
     */
    private $encryptor;
    /**
     * @var AccountManagementInterface
     */
    private $customerAccountManagement;
    /**
     * @var AddressInterfaceFactory
     */
    private $addressFactory;
    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;
    /**
     * @var CustomerFactory
     */
    private $customerFactory;
    /**
     * @var DirectoryCountryRegionTownRepositoryInterface
     */
    private $countryRegionTownRepository;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var HorecaContactRepositoryInterface
     */
    private $horecaContactRepository;

    /**
     * Search constructor.
     *
     * @param Context                                       $context
     * @param Session                                       $customerSession
     * @param SerializerJson                                $serializerJson
     * @param JsonFactory                                   $resultFactory
     * @param Logger                                        $logger
     * @param RepositoryInterface                           $modelRepository
     * @param ModelFactory                                  $modelFactory
     * @param CustomerFactory                               $customerFactory
     * @param Encryptor                                     $encryptor
     * @param AccountManagementInterface                    $customerAccountManagement
     * @param AddressInterfaceFactory                       $addressFactory
     * @param AddressRepositoryInterface                    $addressRepository
     * @param DirectoryCountryRegionTownRepositoryInterface $countryRegionTownRepository
     * @param CustomerRepositoryInterface                   $customerRepository
     * @param HorecaContactRepositoryInterface              $horecaContactRepository
     * @param Data                                          $culturaMezcal
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        SerializerJson $serializerJson,
        JsonFactory $resultFactory,
        Logger $logger,
        RepositoryInterface $modelRepository,
        ModelFactory $modelFactory,
        CustomerFactory $customerFactory,
        Encryptor $encryptor,
        AccountManagementInterface $customerAccountManagement,
        AddressInterfaceFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        DirectoryCountryRegionTownRepositoryInterface $countryRegionTownRepository,
        CustomerRepositoryInterface $customerRepository,
        HorecaContactRepositoryInterface $horecaContactRepository,
        Data $culturaMezcal
    ) {
        parent::__construct($context);
        $this->resultFactory               = $resultFactory;
        $this->modelRepository             = $modelRepository;
        $this->modelFactory                = $modelFactory;
        $this->customerSession             = $customerSession;
        $this->serializerJson              = $serializerJson;
        $this->messageManager              = $context->getMessageManager();
        $this->logger                      = $logger;
        $this->culturaMezcal               = $culturaMezcal;
        $this->encryptor                   = $encryptor;
        $this->customerAccountManagement   = $customerAccountManagement;
        $this->addressFactory              = $addressFactory;
        $this->addressRepository           = $addressRepository;
        $this->customerFactory             = $customerFactory;
        $this->countryRegionTownRepository = $countryRegionTownRepository;
        $this->customerRepository          = $customerRepository;
        $this->horecaContactRepository     = $horecaContactRepository;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->resultFactory->create();

        /**
         * If the customer is not logged in the service return an empty response
         */
        $forwardId = null;
        if ($this->customerSession->isLoggedIn()) {
            if (!$this->culturaMezcal->isForward()) {
                $result->setData([
                    'redirect' => '/customer/account'
                ]);

                return $result;
            } else {
                $forwardId = $this->customerSession->getId();
            }
        }

        $data = $this->serializerJson->unserialize($this->getRequest()->getContent());

        try {

            $this->modelRepository->get($data['horeca']['email']);

            $result->setData(['ok' => false]);
            $this->messageManager->addWarningMessage(__('Already exists an account with that email.'));
        } catch (NoSuchEntityException $e) {
            $horecaCustomer = $this->customerFactory->create();
            $horecaCustomer
                ->setEmail($data['horeca']['email'])
                ->setFirstname($data['horeca']['firstname'])
                ->setLastname($data['horeca']['lastname'])
                ->setGroupId($this->culturaMezcal->getHorecaCustomerGroupId())
                ->setStoreId($this->culturaMezcal->getCurrentStoreId())
                ->setWebsiteId($this->culturaMezcal->getCurrentWebsiteId());

            $extraAttributes = [
                'firstname',
                'lastname',
                'web',
                'phone',
                'address_street',
                'external_number',
                'internal_number',
                'zip_code',
                'country_id',
                'region_id',
                'town_id',
                'cif',
                'iban',
                'direct_debit_term'
            ];

            foreach ($extraAttributes as $extraAttribute) {
                $horecaCustomer->setCustomAttribute($extraAttribute, $data['horeca'][$extraAttribute] ?? null);
            }

            if (empty($forwardId)) {
                /**
                 * SI NO HAY forward_id QUIERE DECIR QUE ES UN AUTOREGISTRO Y LA CUENTA DEBE SER APROVADA
                 */
                $horecaCustomer->setCustomAttribute(
                    'horeca_status',
                    $this->culturaMezcal->getAttribtueOptionValueByLabel('horeca_status', 'Pending')
                );
                $this->modelRepository->save($horecaCustomer, $this->encryptor->getHash($data['horeca']['password'], true));
                $horecaCustomer = $this->modelRepository->get($horecaCustomer->getEmail());
                $this->createAddress($horecaCustomer, $data);
                $this->createContacts($horecaCustomer, $data);

                $result->setData(
                    [
                        'ok'       => true,
                        'redirect' => $this->_url->getUrl('customer/account/login')
                    ]
                );
                $this->messageManager->addSuccessMessage(__('Congratulations your account has been created, an administrator will validate your information before you can use your account.'));

            } else {
                /**
                 * SI HAY UN forward_id, QUIERE DECIR QUE ES UN REGISTRO DE UN HORECA
                 */
                $horecaCustomer->setCustomAttribute(
                    'horeca_status',
                    $this->culturaMezcal->getAttribtueOptionValueByLabel('horeca_status', 'Registered by Forward')
                );

                $horecaCustomer->setCustomAttribute('forward_id', $forwardId);
                $this->modelRepository->save($horecaCustomer, $this->encryptor->getHash($data['horeca']['password'], true));
                $horecaCustomer = $this->modelRepository->get($horecaCustomer->getEmail());

                $this->createAddress($horecaCustomer, $data);
                $this->createContacts($horecaCustomer, $data);

                $result->setData(
                    [
                        'ok' => true,
                    ]
                );
                $this->messageManager->addSuccessMessage(__('Congratulations your HORECA has been saved.'));
            }


        } catch (Exception $e) {
            $result->setData(['ok' => false, 'exception' => $e->getMessage()]);
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong saving the forward.'));
        }

        return $result;

    }

    /**
     * @param CustomerInterface $customer
     * @param                   $data
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function createAddress(CustomerInterface $customer, $data)
    {

        $address = $this->addressFactory->create();
        $address
            ->setCustomerId($customer->getId())
            ->setStreet([$data['shipping']['address_street']])
            ->setTelephone($data['horeca']['phone'])
            ->setCountryId($data['shipping']['country_id'])
            ->setRegionId($data['shipping']['region_id'])
            ->setPostcode($data['shipping']['zip_code'])
            ->setFirstname($data['horeca']['firstname'])
            ->setLastname($data['horeca']['lastname'])
            ->setCity(
                $this->countryRegionTownRepository->load($data['shipping']['town_id'])->getName()
            );

        $address->isDefaultShipping(true);

        $address->setCustomAttribute('external_number', $data['shipping']['external_number']);
        $address->setCustomAttribute('internal_number', $data['shipping']['internal_number']);
        $address->setCustomAttribute('town_id', $data['shipping']['town_id']);
        $address->setCustomAttribute('delivery_days_id', $data['shipping']['delivery_days_id']);
        $address->setCustomAttribute('delivery_time', $data['shipping']['delivery_time']);

        $this->addressRepository->save($address);
        $customer->setAddresses([
            $address
        ]);
        $customer->setDefaultShipping($address->getId());
        $this->customerRepository->save($customer);
    }

    private function createContacts(CustomerInterface $customer, $data)
    {
        foreach ($data['contacts'] as $contact) {
            $newContact = $this->horecaContactRepository->create();
            $newContact
                ->setName($contact['name'])
                ->setHorecaId($customer->getId())
                ->setJobTitle($contact['job_title'])
                ->setEmail($contact['email'])
                ->setPhone($contact['phone']);
            $this->horecaContactRepository->save($newContact);
        }
    }
}
