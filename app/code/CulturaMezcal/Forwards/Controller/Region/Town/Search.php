<?php

namespace CulturaMezcal\Forwards\Controller\Region\Town;

use CulturaMezcal\Forwards\Logger\Logger;
use CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\CollectionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

class Search extends Action
{

    /**
     * @var SerializerJson $serializerJson
     */
    protected $serializerJson;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var CollectionFactory
     */
    private $regionsFactory;

    /**
     * Search constructor.
     *
     * @param Context           $context
     * @param SerializerJson    $serializerJson
     * @param JsonFactory       $resultFactory
     * @param CollectionFactory $regionsFactory
     * @param Logger            $logger
     */
    public function __construct(
        Context $context,
        SerializerJson $serializerJson,
        JsonFactory $resultFactory,
        CollectionFactory $regionsFactory,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->resultFactory  = $resultFactory;
        $this->serializerJson = $serializerJson;
        $this->messageManager = $context->getMessageManager();
        $this->logger         = $logger;
        $this->regionsFactory = $regionsFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        /**
         * This is to create the response as JSON
         */
        $result = $this->resultFactory->create();

        try {
            $request = $this->getRequest()->getContent();
            /*if (empty($request)) {
                $result->setData(['ok' => true, 'options' => []]);
            } else {*/
            $data    = empty($request) ? [] : $this->serializerJson->unserialize($request);
            /** @var \CulturaMezcal\Forwards\Model\ResourceModel\DirectoryCountryRegionTown\Collection $collection */
            $collection = $this->regionsFactory->create();
            $options    = $collection->addFieldToFilter('region_id', $data['id'] ?? '131')->load()->toOptionArray();
            $result->setData(['ok' => true, 'options' => $options]);
            //}
        } catch (\Exception $e) {
            $result->setData(['ok' => $e->getMessage()]);
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong saving the forward.'));
        }

        return $result;
    }
}
