<?php

namespace CulturaMezcal\Forwards\Controller\Adminhtml\Horeca\Contact;

use CulturaMezcal\Forwards\Api\Data\HorecaContactInterface as DataInterface;
use CulturaMezcal\Forwards\Api\HorecaContactRepositoryInterface as RepositoryInterface;
use CulturaMezcal\Forwards\Logger\Logger;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var DataInterface
     */
    protected $model;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @param Action\Context      $context
     * @param PageFactory         $resultPageFactory
     * @param RepositoryInterface $repository
     * @param DataInterface       $model ,
     * @param Logger              $logger
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        RepositoryInterface $repository,
        DataInterface $model,
        Logger $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->repository        = $repository;
        $this->model             = $model;
        $this->logger            = $logger;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('{{{module_name_underscore}}}::Contact')
            ->addBreadcrumb(__('CulturaMezcal Forwards'), __('CulturaMezcal Forwards'))
            ->addBreadcrumb(__('Manage Item'), __('Manage Item'));

        return $resultPage;
    }

    /**
     * Edit Item
     *
     * @return \Magento\Framework\Controller\AbstractResult
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {

        /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {

            $id = $this->getRequest()->getParam('id');

            if ($id) {
                $this->repository->loadModel($this->model, $id);
                if (!$this->model->getId()) {
                    $this->messageManager->addErrorMessage(__('This item no longer exists.'));

                    return $resultRedirect->setPath('*/*/');
                }
            }

            if (!empty($data = $this->_session->getFormData())) {
                $this->model
                    ->setHorecaId($data['horeca_id'] ?? null)
                    ->setName($data['name'] ?? null)
                    ->setJobTitle($data['job_title'] ?? null)
                    ->setPhone($data['phone'] ?? null);
            }

            /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
            $resultPage = $this->_initAction();
            $resultPage->addBreadcrumb(__('CulturaMezcal'), __('CulturaMezcal'));

            $name   = $this->model->getId();
            $label  = __($id ? 'Edit %1 - %2' : 'New %1', 'Contact', $name);
            $prefix = $title = $label;

            $resultPage->addBreadcrumb($label, $title);
            $resultPage->getConfig()->getTitle()->prepend($prefix);

            return $resultPage;

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Contact'));
        } catch (\RuntimeException $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Contact'));
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Contact'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
