<?php

namespace CulturaMezcal\Forwards\Controller\Adminhtml\Horeca\Contact;

use CulturaMezcal\Forwards\Api\HorecaContactRepositoryInterface as RepositoryInterface;
use CulturaMezcal\Forwards\Logger\Logger;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var CulturaMezcal\Forwards\Api\HorecaContactRepositoryInterface
     */
    protected $repository;
    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @param Action\Context      $context
     * @param RepositoryInterface $repository
     * @param Logger              $logger
     */
    public function __construct(
        Action\Context $context,
        RepositoryInterface $repository,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->repository = $repository;
        $this->logger     = $logger;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $model = $this->repository->create('id');

            $id = $this->getRequest()->getParam('id');

            if ($id) {
                $this->repository->loadModel($model, $id);
            }

            try {

                $model
                    ->setHorecaId($data['horeca_id'] ?? null)
                    ->setName($data['name'] ?? null)
                    ->setJobTitle($data['job_title'] ?? null)
                    ->setPhone($data['phone'] ?? null)
                    ->setEmail($data['email'] ?? null);
                $this->repository->save($model);

                if ($model->getId()) {
                    $this->messageManager->addSuccessMessage(__('Contact has been saved.'));
                    $this->_session->setData('culturamezcal_forwards_horeca_contact_form_data', false);

                    return $resultRedirect->setPath('customer/index/edit', ['id' => $model->getHorecaId()]);

                }

                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Contact'));

                $this->_session->setData('culturamezcal_forwards_horeca_contact_form_data', $data);

                return $resultRedirect->setPath('*/*/new');

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Contact'));
            } catch (\RuntimeException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Contact'));
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Contact'));
            }

            $this->_session->setData('culturamezcal_forwards_horeca_contact_form_data', $data);

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
