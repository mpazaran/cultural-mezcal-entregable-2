<?php

namespace CulturaMezcal\Forwards\Controller\Adminhtml\Export;

use Magento\Ui\Controller\Adminhtml\Export\GridToXml as MagentoGridToXml;

/**
 * Controller to perform export
 */
class GridToXml extends MagentoGridToXml
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'CulturaMezcal_Forwards::horeca_contact_read';

}
