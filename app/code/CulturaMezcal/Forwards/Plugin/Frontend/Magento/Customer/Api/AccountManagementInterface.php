<?php

namespace CulturaMezcal\Forwards\Plugin\Frontend\Magento\Customer\Api;

use CulturaMezcal\Core\Helper\Data;
use Magento\Framework\Exception\LocalizedException;

/**
 * Used in order to and unapproved account can be used
 *
 * @package CulturaMezcal\Forwards\Plugin\Frontend\Magento\Customer\Api
 */
class AccountManagementInterface
{
    /**
     * @var Data
     */
    private $culturaMezcal;

    /**
     * AccountManagementInterface constructor.
     *
     * @param Data $culturaMezcal
     */
    public function __construct(
        Data $culturaMezcal
    ) {

        $this->culturaMezcal = $culturaMezcal;
    }

    /**
     * Authenticate a customer
     *
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Customer\Api\Data\CustomerInterface     $result
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterAuthenticate(
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        $result
    ) {

        if ($result->getGroupId() == $this->culturaMezcal->getForwardCustomerGroupId()) {
            if (!$this->culturaMezcal->isForwardApproved($result)) {
                $this->culturaMezcal->getMessageManager()->addErrorMessage($message = __('Your account is not approved yet!.'));
                throw new LocalizedException($message);
            }
        } else {
            if ($result->getGroupId() == $this->culturaMezcal->getHorecaCustomerGroupId()) {
                if (!$this->culturaMezcal->isHorecaApproved($result)) {
                    $this->culturaMezcal->getMessageManager()->addErrorMessage($message = __('Your account is not approved yet!.'));
                    throw new LocalizedException($message);
                }
            }
        }

        return $result;
    }
}

