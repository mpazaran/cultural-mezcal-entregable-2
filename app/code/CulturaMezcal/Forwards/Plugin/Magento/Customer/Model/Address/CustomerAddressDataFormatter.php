<?php

namespace CulturaMezcal\Forwards\Plugin\Magento\Customer\Model\Address;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\Address\CustomerAddressDataFormatter as MagentoCustomerAddressDataFormatter;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;

class CustomerAddressDataFormatter
{

    /**
     * @var CollectionFactory
     */
    private $addressCollectionFactory;
    /**
     * @var \CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface
     */
    private $regionTownRepository;
    /**
     * @var \CulturaMezcal\Core\Helper\Data
     */
    private $culturaMezcal;

    /**
     * CustomerAddressDataFormatter constructor.
     *
     * @param CollectionFactory                                                         $addressCollectionFactory
     * @param \CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface $regionTownRepository
     * @param \CulturaMezcal\Core\Helper\Data                                           $culturaMezcal
     */
    public function __construct(
        CollectionFactory $addressCollectionFactory,
        \CulturaMezcal\Forwards\Api\DirectoryCountryRegionTownRepositoryInterface $regionTownRepository,
        \CulturaMezcal\Core\Helper\Data $culturaMezcal
    ) {
        $this->addressCollectionFactory = $addressCollectionFactory;
        $this->regionTownRepository     = $regionTownRepository;
        $this->culturaMezcal            = $culturaMezcal;
    }

    /**
     * @param MagentoCustomerAddressDataFormatter $addressDataFormatter
     * @param                                     $resultAddress
     * @param AddressInterface                    $customerAddress
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterPrepareAddress(
        MagentoCustomerAddressDataFormatter $addressDataFormatter,
        $resultAddress,
        AddressInterface $customerAddress
    ) {

        $customerAddressModel = $this->addressCollectionFactory
            ->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $customerAddress->getId())
            ->getFirstItem();

        if ($customerAddressModel->getTownId()) {
            $region     = $this->regionTownRepository->load($customerAddressModel->getTownId());
            $regionName = $region->getName();
        }


        /* @var \Magento\Customer\Api\Data\AddressExtension $extensionAttributes */
        $resultAddress['custom_attributes'] = [
            'notes'              => $customerAddressModel->getNotes(),
            'external_number'    => $customerAddressModel->getExternalNumber(),
            'internal_number'    => $customerAddressModel->getInternalNumber(),
            'delivery_days'      => $customerAddressModel->getDeliveryDays(),
            'delivery_days_text' => implode(', ', $this->culturaMezcal->getAttributeLabelByIds('delivery_days', $customerAddressModel->getDeliveryDays())),
            'delivery_time'      => $customerAddressModel->getDeliveryTime(),
            'delivery_time_text' => implode(', ', $this->culturaMezcal->getAttributeLabelByIds('delivery_time', $customerAddressModel->getDeliveryTime())),
            'town_id'            => $customerAddressModel->getTownId(),
            'town_name'          => $regionName ?? '',
        ];

        return $resultAddress;
    }

}
