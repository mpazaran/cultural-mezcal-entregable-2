<?php

namespace CulturaMezcal\Forwards\Model;

use CulturaMezcal\Core\Helper\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\AccountConfirmation;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\Config\Share;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;
use Magento\Customer\Model\ResourceModel\Customer as ResourceCustomer;
use Magento\Customer\Model\Session;
use Magento\Eav\Model\Config;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Math\Random;
use Magento\Framework\Model\Context;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @package CulturaMezcal\Customer\Model
 */
class Customer extends \Magento\Customer\Model\Customer
{
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var Data
     */
    private $culturaMezcal;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * Customer constructor.
     *
     * @param Context                   $context
     * @param Registry                  $registry
     * @param StoreManagerInterface     $storeManager
     * @param Config                    $config
     * @param ScopeConfigInterface      $scopeConfig
     * @param ResourceCustomer          $resource
     * @param Share                     $configShare
     * @param AddressFactory            $addressFactory
     * @param CollectionFactory         $addressesFactory
     * @param TransportBuilder          $transportBuilder
     * @param GroupRepositoryInterface  $groupRepository
     * @param EncryptorInterface        $encryptor
     * @param DateTime                  $dateTime
     * @param CustomerInterfaceFactory  $customerDataFactory
     * @param DataObjectProcessor       $dataObjectProcessor
     * @param DataObjectHelper          $dataObjectHelper
     * @param CustomerMetadataInterface $metadataService
     * @param IndexerRegistry           $indexerRegistry
     * @param Data                      $culturaMezcal
     * @param Session                   $customerSession
     * @param CheckoutSession           $checkoutSession
     * @param AbstractDb|null           $resourceCollection
     * @param array                     $data
     * @param AccountConfirmation|null  $accountConfirmation
     * @param Random|null               $mathRandom
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        Config $config,
        ScopeConfigInterface $scopeConfig,
        ResourceCustomer $resource,
        Share $configShare,
        AddressFactory $addressFactory,
        CollectionFactory $addressesFactory,
        TransportBuilder $transportBuilder,
        GroupRepositoryInterface $groupRepository,
        EncryptorInterface $encryptor,
        DateTime $dateTime,
        CustomerInterfaceFactory $customerDataFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        CustomerMetadataInterface $metadataService,
        IndexerRegistry $indexerRegistry,
        Data $culturaMezcal,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        AbstractDb $resourceCollection = null,
        array $data = [],
        AccountConfirmation $accountConfirmation = null,
        Random $mathRandom = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $storeManager,
            $config,
            $scopeConfig,
            $resource,
            $configShare,
            $addressFactory,
            $addressesFactory,
            $transportBuilder,
            $groupRepository,
            $encryptor,
            $dateTime,
            $customerDataFactory,
            $dataObjectProcessor,
            $dataObjectHelper,
            $metadataService,
            $indexerRegistry,
            $resourceCollection,
            $data,
            $accountConfirmation,
            $mathRandom
        );
        $this->customerSession = $customerSession;
        $this->customerSession->setData('horeca_id', 1);
        $this->culturaMezcal   = $culturaMezcal;
        $this->checkoutSession = $checkoutSession;
    }

    public function getAddressesCollection()
    {
        $customerGroup = $this->getGroupId();

        $horecaId = $this->checkoutSession->getHorecaId();
        if ($this->getId() == $horecaId || $customerGroup != $this->culturaMezcal->getForwardCustomerGroupId()) {
            return parent::getAddressCollection();
        }
        /** @var \Magento\Customer\Model\Data\Customer $horeca */
        $horeca = $this->getCollection()->addFieldToFilter('entity_id', $horecaId)->getFirstItem();
        if ($this->_addressesCollection === null) {
            $this->_addressesCollection = $this->getAddressCollection()->setCustomerFilter(
                $horeca
            )->addAttributeToSelect(
                '*'
            );
            foreach ($this->_addressesCollection as $address) {
                $address->setCustomer($horeca);
            }
        }

        return $this->_addressesCollection;
    }
}
