<?php

namespace CulturaMezcal\Forwards\Model\Export;

use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Ui\Model\Export\MetadataProvider as MagentoMetadataProvider;

class MetadataProvider extends MagentoMetadataProvider
{
    /**
     * @param \Magento\Framework\View\Element\UiComponentInterface $component
     *
     * @return array
     */
    public function getFields(\Magento\Framework\View\Element\UiComponentInterface $component)
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            if($column->getData('export') === 'false'){
                continue;
            }
            if (null !== ($exportKey = $column->getData('exportIndex'))) {
                $row[] = $exportKey;
            } else {
                $row[] = $column->getName();
            }

        }
        return $row;
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @param UiComponentInterface $component
     * @return string[]
     */
    public function getHeaders(UiComponentInterface $component)
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            if($column->getData('export') === 'false'){
                continue;
            }
            $row[] = $column->getData('config/label');
        }

        array_walk($row, function (&$header) {
            if (mb_strpos($header, 'ID') === 0) {
                $header = '"' . $header . '"';
            }
        });

        return $row;
    }
}
