<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel\Horeca\Contact;

use CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact as ResourceModel;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Psr\Log\LoggerInterface as Logger;

class ListCollection extends SearchResult
{

    /**
     * ListCollection constructor.
     *
     * @param EntityFactory    $entityFactory
     * @param Logger           $logger
     * @param FetchStrategy    $fetchStrategy
     * @param EventManager     $eventManager
     * @param ResourceModel    $resourceModel
     * @param RequestInterface $request
     * @param null             $identifierName
     * @param null             $connectionName
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        ResourceModel $resourceModel,
        RequestInterface $request,
        $identifierName = null,
        $connectionName = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $resourceModel->getMainTable(),
            ResourceModel::class,
            $identifierName,
            $connectionName
        );
        $this->addFieldToFilter('horeca_id', $request->getParam('horeca_id'));
    }

}
