<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel;

/**
 * Relates the mysql table directory_country_region_town with the model \CulturaMezcal\Forwards\Model\DirectoryCountryRegionTown.
 *
 */
class DirectoryCountryRegionTown extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritdoc
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('directory_country_region_town', 'id');
    }
}

