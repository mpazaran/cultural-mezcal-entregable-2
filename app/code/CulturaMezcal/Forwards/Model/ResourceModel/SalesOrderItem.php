<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel;

/**
 * Relates the mysql table sales_order_item with the model \CulturaMezcal\Forwards\Model\SalesOrderItem.
 *
 */
class SalesOrderItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritdoc
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('sales_order_item', 'item_id');
    }
}

