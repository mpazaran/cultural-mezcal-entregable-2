<?php

namespace CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact;

/**
 * Collection for \CulturaMezcal\Forwards\Model\HorecaContact.
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_forwards_horeca_contact_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'cultura_mezcal_forwards_horeca_contact_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\CulturaMezcal\Forwards\Model\HorecaContact::class, \CulturaMezcal\Forwards\Model\ResourceModel\HorecaContact::class);
    }

}


