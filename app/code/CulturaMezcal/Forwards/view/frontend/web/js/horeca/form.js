define([
    "uiComponent",
    "uiRegistry",
    "mage/storage",
    "mage/translate",
    "mage/validation",
    "Magento_Ui/js/modal/alert",
    "Magento_Ui/js/modal/confirm",
    "jquery",
    "ko",
    "CulturaMezcal_Core/js/messages",
    "mage/calendar",
    "mage/loader"
], function (Component, registry, storage, __, validation, alert, confirm, $, ko, messagesContainer) {
    let messages = messagesContainer();
    return Component.extend({

        defaults      : {
            template       : {
                name       : "CulturaMezcal_Forwards/horeca/form",
                afterRender: function (renderedNodesArray, data) {
                    data.form = $('#forwards_register_form');
                    data.form.validation();
                    $("#dob").calendar({
                        showsTime  : false,
                        dateFormat : "Y-MM-dd",
                        buttonImage: "",
                        yearRange  : "-60y:c+nn",
                        buttonText : "Select Date", maxDate: "-17y", changeMonth: true, changeYear: true, showOn: "both", firstDay: 1
                    });
                    $(".ui-datepicker-trigger.v-middle").remove();
                }
            },
            showForm       : ko.observable(false),
            model          : ko.observable(null),
            loading        : ko.observable(false),
            countries      : ko.observableArray([]),
            shippingRegions: ko.observableArray([]),
            regions        : ko.observableArray([]),
            towns          : ko.observableArray([]),
            shippingTowns  : ko.observableArray([]),
            spainRegions   : ko.observableArray([]),
            deliveryTimes  : ko.observableArray([]),
            deliveryDays   : ko.observableArray([]),
            directDebitTerm: ko.observableArray([]),
            form           : null,
            list           : undefined,
            horeca         : {
                email            : ko.observable(),
                password         : ko.observable(),
                firstname        : ko.observable(),
                lastname         : ko.observable(),
                web              : ko.observable(),
                phone            : ko.observable(),
                address_street   : ko.observable(),
                external_number  : ko.observable(),
                internal_number  : ko.observable(),
                zip_code         : ko.observable(),
                country_id       : ko.observable('ES'),
                region_id        : ko.observable(),
                town_id          : ko.observable(),
                cif              : ko.observable(),
                iban             : ko.observable(),
                direct_debit_term: ko.observable(),
            },
            shipping       : {
                delivery_time   : ko.observableArray([]),
                delivery_days_id: ko.observableArray([]),
                address_street  : ko.observable(),
                external_number : ko.observable(),
                internal_number : ko.observable(),
                zip_code        : ko.observable(),
                country_id      : ko.observable('ES'),
                region_id       : ko.observable(),
                town_id         : ko.observable(),
            },
            contacts       : ko.observableArray([]),
        },
        initialize    : function () {
            window.lalal = this;
            this._super();
            let body = $('body');
            this.loading.subscribe(function (value) {
                if (value) {
                    body.trigger('processStart');
                } else {
                    body.trigger('processStop');
                }
            }.bind(this));
            this.horeca.country_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.regions(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.shipping.country_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.shippingRegions(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.shipping.region_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region_town/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.shippingTowns(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));
            this.horeca.region_id.subscribe(function (id) {
                this.loading(true);
                storage.post(
                    "forwards/region_town/search",
                    JSON.stringify({id: id})
                ).done(
                    function (response) {
                        if (response.ok) {
                            this.towns(response.options);
                        }
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }.bind(this));

            this.list = registry.get('horeca_list');
            if (this.list != undefined) {
                this.list.showForm.subscribe(function(newValue){this.showList(!newValue)}.bind(this.list));
                this.list.form = this;
            }
        },
        initObservable: function () {
            var result = this._super()
                .observe([
                    'countries',
                    'shippingRegions',
                    'spainRegions',
                    'deliveryDays',
                    'deliveryTimes',
                    'towns',
                    'shippingTowns',
                    'contacts',
                    'showForm'
                ]);

            this.shippingRegions(this.spainRegions());
            this.regions(this.spainRegions());
            return result;
        },
        save          : function (back) {
            return function () {
                if (this.form.validation('isValid')) {
                    this.loading(true);
                    storage.post(
                        "forwards/horeca/save",
                        ko.toJSON(
                            {
                                horeca  : this.horeca,
                                shipping: this.shipping,
                                contacts: this.contacts
                            }
                        )
                    ).done(
                        function (response) {
                            if (response.ok) {
                                if (this.list !== undefined) {
                                    this.list.loadModels();
                                    this.showForm(false);
                                }
                            }
                        }.bind(this)
                    ).fail(
                        function (response) {
                        }.bind(this)
                    ).complete(
                        function (response) {
                            if (undefined !== response.responseJSON.redirect) {
                                this.loading(true);
                                window.location = response.responseJSON.redirect;
                            } else {
                                this.loading(false);
                            }
                            messages.timedClear();
                        }.bind(this)
                    );
                }
            }.bind(this);
        },
        addContact    : function () {
            return function () {
                this.contacts.push({
                    id       : ko.observable(null),
                    name     : ko.observable(''),
                    job_title: ko.observable(''),
                    phone   : ko.observable(''),
                    email    : ko.observable('')
                });
            }.bind(this);
        },
        removeContact : function (data) {
            return function () {
                this.contacts.remove(data);
            }.bind(this);
        },
        cancel        : function () {
            return function () {
                this.showForm(false);
            }.bind(this);
        }
    });
});
