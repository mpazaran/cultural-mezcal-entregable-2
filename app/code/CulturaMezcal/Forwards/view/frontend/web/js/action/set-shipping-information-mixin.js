/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'uiRegistry'
], function ($, wrapper, quote, registry) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {

            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            let checkoutShippingAddress = undefined === shippingAddress.customerAddressId ?
                registry.get('checkoutProvider').shippingAddress :
                customerData.addresses[shippingAddress.customerAddressId];

            shippingAddress['extension_attributes']['notes'] = checkoutShippingAddress.custom_attributes.notes;
            shippingAddress['extension_attributes']['external_number'] = checkoutShippingAddress.custom_attributes.external_number;
            shippingAddress['extension_attributes']['internal_number'] = checkoutShippingAddress.custom_attributes.internal_number;
            shippingAddress['extension_attributes']['delivery_days'] = checkoutShippingAddress.custom_attributes.delivery_days;
            shippingAddress['extension_attributes']['delivery_time'] = checkoutShippingAddress.custom_attributes.delivery_time;
            shippingAddress['extension_attributes']['town_id'] = checkoutShippingAddress.custom_attributes.town_id;

            return originalAction();
        });
    };
});
