define(
    [
        'jquery',
        'ko',
        "mage/storage",
        'Magento_Checkout/js/view/shipping'
    ],
    function (
        $,
        ko,
        storage,
        Component
    ) {
        'use strict';
        return Component.extend({
            isForwardPresent : ko.observable(false),
            isFakeLogin      : ko.observable(false),
            loading          : ko.observable(false),
            defaults         : {
                template: 'CulturaMezcal_Forwards/shipping'
            },
            initialize       : function () {
                var self = this;
                this._super();
                self.isForwardPresent.subscribe(function (isPresent) {
                    self.setForwardPresent(isPresent);
                });
                let body = $('body');
                this.loading.subscribe(function (value) {
                    if (value) {
                        body.trigger('processStart');
                    } else {
                        body.trigger('processStop');
                    }
                }.bind(this));
            },
            setForwardPresent: function (isPresent) {
                this.loading(true);
                storage.post(
                    "forwards/checkout/forwardIsPresent",
                    JSON.stringify({
                        'present': isPresent ? '1' : '0'
                    })
                ).done(
                    function (response) {
                    }.bind(this)
                ).fail(
                    function (response) {
                    }.bind(this)
                ).complete(
                    function (response) {
                        if (undefined !== response.responseJSON.redirect) {
                            window.location = response.responseJSON.redirect;
                        }
                        this.loading(false);
                        messages.timedClear();
                    }.bind(this)
                );
            }
        });
    }
);
