var config = {
    config: {
        "mixins": {
            "Magento_Checkout/js/action/set-shipping-information": {
                "CulturaMezcal_Forwards/js/action/set-shipping-information-mixin": true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'CulturaMezcal_Forwards/js/action/create-shipping-address-mixin': true
            },
        },
        "map"   : {
            "*": {
            }
        }
    }
};

