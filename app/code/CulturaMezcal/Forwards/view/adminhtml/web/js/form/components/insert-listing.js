define([
    'Magento_Ui/js/form/components/insert-listing'
], function (Insert) {
    'use strict';

    return Insert.extend({
        initialize: function () {
            this._super();
            _.bindAll(this, 'updateValue', 'updateExternalValueByEditableData');
            this.renderSettings.url = this.renderSettings.url.replace('/key/', '/horeca_id/' + this.source.data.customer_id + '/key/');
            return this;
        },
    });
})
