<?php

namespace CulturaMezcal\Commissions\Block\Adminhtml\Commisions\Edit;

use CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface as DataInterface;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \CulturaMezcal\Commissions\Api\{{{MODEL}}}Interface
     */
    protected $model;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param DataInterface $model
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        DataInterface $model
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->model = $model;
    }

    /**
     * Return the model Id
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->model ? $this->model->getId() : null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
