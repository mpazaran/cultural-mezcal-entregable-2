<?php

namespace CulturaMezcal\Commissions\Model\ResourceModel\Commisions;

use CulturaMezcal\Commissions\Model\ResourceModel\SalesOrderItem as ResourceModel;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Psr\Log\LoggerInterface as Logger;

class ListCollection extends SearchResult
{
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        ResourceModel $resourceModel,
        $identifierName = null,
        $connectionName = null
    ) {
        parent::__construct($entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $resourceModel->getMainTable(),
            ResourceModel::class,
            $identifierName, $connectionName);
    }

    /**
     * Initialization here
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->addFilterToMap('order_id','main_table.order_id');
        $this->addFilterToMap('order_increment_id','order_table.increment_id');
        $this->addFilterToMap('order_firstname','order_table.customer_firstname');
        $this->addFilterToMap('order_lastname','order_table.customer_lastname');
        $this->addFilterToMap('order_email','order_table.customer_email');
        $this->addFilterToMap('product_sku','product_table.sku');
        $this->addFilterToMap('product_name_value','product_table_name.value');
    }

    /**
     * Here you can write the joins
     * @return SearchResult|void
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinLeft(
            ['order_table' => $this->getTable('sales_order')],
            'main_table.order_id = order_table.entity_id',
            [
                'order_increment_id' => 'increment_id',
                'order_firstname'    => 'customer_firstname',
                'order_lastname'     => 'customer_lastname',
                'order_email'        => 'customer_email',
            ]
        );
        $this->getSelect()->joinLeft(
            ['product_table' => $this->getTable('catalog_product_entity')],
            'main_table.product_id = product_table.entity_id',
            [
                'product_sku'     => 'sku'
            ]
        );
        $this->getSelect()->joinLeft(
            ['product_table_name' => $this->getTable('catalog_product_entity_varchar')],
            'product_table_name.row_id = product_table.row_id and product_table_name.store_id = 0',
            [
                'product_name_value'     => 'product_table_name.value'
            ]
        );
        $this->getSelect()->join(
            ['product_table_ea' => $this->getTable('eav_attribute')],
            'product_table_ea.attribute_id = product_table_name.attribute_id and product_table_ea.attribute_code = \'name\'',
            []
        );
    }

}
