<?php

namespace CulturaMezcal\Commissions\Model\ResourceModel\SalesOrderItem;

/**
 * Collection for \CulturaMezcal\Commissions\Model\SalesOrderItem.
 *
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Identifier field name for collection items
     *
     * Can be used by collections with items without defined
     *
     * @var string
     */
    protected $_idFieldName = 'item_id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_commissions_sales_order_item_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'cultura_mezcal_commissions_sales_order_item_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\CulturaMezcal\Commissions\Model\SalesOrderItem::class, \CulturaMezcal\Commissions\Model\ResourceModel\SalesOrderItem::class);
    }

}


