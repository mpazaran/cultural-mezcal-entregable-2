<?php

namespace CulturaMezcal\Commissions\Model\DataProvider\Commisions;

use CulturaMezcal\Commissions\Helper\Media;
use CulturaMezcal\Commissions\Model\ResourceModel\SalesOrderItem\CollectionFactory as CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;

/**
 * Class FormData
 */
class FormDataSource extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Search\Model\ResourceModel\SynonymGroup\Collection
     */
    protected $collection;

    /**
     * @var FilterPool
     */
    protected $filterPool;

    /**
     * @var array
     */
    protected $loadedData;


    /**
     * Constructor
     *
     * @param string            $name
     * @param string            $primaryFieldName
     * @param string            $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param FilterPool        $filterPool
     * @param Context           $context
     * @param array             $meta
     * @param array             $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        FilterPool $filterPool,
        Context $context,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $blockCollectionFactory->create();
        $this->filterPool = $filterPool;
        $this->session    = $context->getSession();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $item) {
            $data = $item->getData();
            $this->loadedData[$item->getId()] = $data;
        }

        if(empty($this->loadedData) && !empty(($data = $this->session->getData('culturamezcal_commissions_commisions_form_data')))){
            $this->loadedData[null] = $data;
        }

        return $this->loadedData;
    }
}
