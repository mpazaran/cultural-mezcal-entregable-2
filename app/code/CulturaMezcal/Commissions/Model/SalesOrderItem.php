<?php

namespace CulturaMezcal\Commissions\Model;

use CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * This is the table sales_order_item representation in Magento.
 *
 * @package CulturaMezcal\Commissions\Model
 */
class SalesOrderItem extends AbstractModel implements IdentityInterface, SalesOrderItemInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     */
    const CACHE_TAG = 'cultura_mezcal_commissions_sales_order_item';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    protected $_cacheTag = 'cultura_mezcal_commissions_sales_order_item';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_eventPrefix = 'cultura_mezcal_commissions_sales_order_item';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\SalesOrderItem::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }


    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->getData('item_id');
    }

    /**
     * @param int $itemId
     *
     * @return SalesOrderItemInterface
     */
    public function setItemId($itemId)
    {
        return $this->setData('item_id', $itemId);
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData('order_id');
    }

    /**
     * @param int $orderId
     *
     * @return SalesOrderItemInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData('order_id', $orderId);
    }

    /**
     * @return int
     */
    public function getParentItemId()
    {
        return $this->getData('parent_item_id');
    }

    /**
     * @param int $parentItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setParentItemId($parentItemId)
    {
        return $this->setData('parent_item_id', $parentItemId);
    }

    /**
     * @return int
     */
    public function getQuoteItemId()
    {
        return $this->getData('quote_item_id');
    }

    /**
     * @param int $quoteItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setQuoteItemId($quoteItemId)
    {
        return $this->setData('quote_item_id', $quoteItemId);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->getData('store_id');
    }

    /**
     * @param int $storeId
     *
     * @return SalesOrderItemInterface
     */
    public function setStoreId($storeId)
    {
        return $this->setData('store_id', $storeId);
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData('created_at');
    }

    /**
     * @param string $createdAt
     *
     * @return SalesOrderItemInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData('created_at', $createdAt);
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData('updated_at');
    }

    /**
     * @param string $updatedAt
     *
     * @return SalesOrderItemInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData('updated_at', $updatedAt);
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getData('product_id');
    }

    /**
     * @param int $productId
     *
     * @return SalesOrderItemInterface
     */
    public function setProductId($productId)
    {
        return $this->setData('product_id', $productId);
    }

    /**
     * @return string
     */
    public function getProductType()
    {
        return $this->getData('product_type');
    }

    /**
     * @param string $productType
     *
     * @return SalesOrderItemInterface
     */
    public function setProductType($productType)
    {
        return $this->setData('product_type', $productType);
    }

    /**
     * @return string
     */
    public function getProductOptions()
    {
        return $this->getData('product_options');
    }

    /**
     * @param string $productOptions
     *
     * @return SalesOrderItemInterface
     */
    public function setProductOptions($productOptions)
    {
        return $this->setData('product_options', $productOptions);
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->getData('weight');
    }

    /**
     * @param float $weight
     *
     * @return SalesOrderItemInterface
     */
    public function setWeight($weight)
    {
        return $this->setData('weight', $weight);
    }

    /**
     * @return int
     */
    public function getIsVirtual()
    {
        return $this->getData('is_virtual');
    }

    /**
     * @param int $isVirtual
     *
     * @return SalesOrderItemInterface
     */
    public function setIsVirtual($isVirtual)
    {
        return $this->setData('is_virtual', $isVirtual);
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->getData('sku');
    }

    /**
     * @param string $sku
     *
     * @return SalesOrderItemInterface
     */
    public function setSku($sku)
    {
        return $this->setData('sku', $sku);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     *
     * @return SalesOrderItemInterface
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData('description');
    }

    /**
     * @param string $description
     *
     * @return SalesOrderItemInterface
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }

    /**
     * @return string
     */
    public function getAppliedRuleIds()
    {
        return $this->getData('applied_rule_ids');
    }

    /**
     * @param string $appliedRuleIds
     *
     * @return SalesOrderItemInterface
     */
    public function setAppliedRuleIds($appliedRuleIds)
    {
        return $this->setData('applied_rule_ids', $appliedRuleIds);
    }

    /**
     * @return string
     */
    public function getAdditionalData()
    {
        return $this->getData('additional_data');
    }

    /**
     * @param string $additionalData
     *
     * @return SalesOrderItemInterface
     */
    public function setAdditionalData($additionalData)
    {
        return $this->setData('additional_data', $additionalData);
    }

    /**
     * @return int
     */
    public function getIsQtyDecimal()
    {
        return $this->getData('is_qty_decimal');
    }

    /**
     * @param int $isQtyDecimal
     *
     * @return SalesOrderItemInterface
     */
    public function setIsQtyDecimal($isQtyDecimal)
    {
        return $this->setData('is_qty_decimal', $isQtyDecimal);
    }

    /**
     * @return int
     */
    public function getNoDiscount()
    {
        return $this->getData('no_discount');
    }

    /**
     * @param int $noDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setNoDiscount($noDiscount)
    {
        return $this->setData('no_discount', $noDiscount);
    }

    /**
     * @return float
     */
    public function getQtyBackordered()
    {
        return $this->getData('qty_backordered');
    }

    /**
     * @param float $qtyBackordered
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyBackordered($qtyBackordered)
    {
        return $this->setData('qty_backordered', $qtyBackordered);
    }

    /**
     * @return float
     */
    public function getQtyCanceled()
    {
        return $this->getData('qty_canceled');
    }

    /**
     * @param float $qtyCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyCanceled($qtyCanceled)
    {
        return $this->setData('qty_canceled', $qtyCanceled);
    }

    /**
     * @return float
     */
    public function getQtyInvoiced()
    {
        return $this->getData('qty_invoiced');
    }

    /**
     * @param float $qtyInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyInvoiced($qtyInvoiced)
    {
        return $this->setData('qty_invoiced', $qtyInvoiced);
    }

    /**
     * @return float
     */
    public function getQtyOrdered()
    {
        return $this->getData('qty_ordered');
    }

    /**
     * @param float $qtyOrdered
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyOrdered($qtyOrdered)
    {
        return $this->setData('qty_ordered', $qtyOrdered);
    }

    /**
     * @return float
     */
    public function getQtyRefunded()
    {
        return $this->getData('qty_refunded');
    }

    /**
     * @param float $qtyRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyRefunded($qtyRefunded)
    {
        return $this->setData('qty_refunded', $qtyRefunded);
    }

    /**
     * @return float
     */
    public function getQtyShipped()
    {
        return $this->getData('qty_shipped');
    }

    /**
     * @param float $qtyShipped
     *
     * @return SalesOrderItemInterface
     */
    public function setQtyShipped($qtyShipped)
    {
        return $this->setData('qty_shipped', $qtyShipped);
    }

    /**
     * @return float
     */
    public function getBaseCost()
    {
        return $this->getData('base_cost');
    }

    /**
     * @param float $baseCost
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseCost($baseCost)
    {
        return $this->setData('base_cost', $baseCost);
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->getData('price');
    }

    /**
     * @param float $price
     *
     * @return SalesOrderItemInterface
     */
    public function setPrice($price)
    {
        return $this->setData('price', $price);
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->getData('base_price');
    }

    /**
     * @param float $basePrice
     *
     * @return SalesOrderItemInterface
     */
    public function setBasePrice($basePrice)
    {
        return $this->setData('base_price', $basePrice);
    }

    /**
     * @return float
     */
    public function getOriginalPrice()
    {
        return $this->getData('original_price');
    }

    /**
     * @param float $originalPrice
     *
     * @return SalesOrderItemInterface
     */
    public function setOriginalPrice($originalPrice)
    {
        return $this->setData('original_price', $originalPrice);
    }

    /**
     * @return float
     */
    public function getBaseOriginalPrice()
    {
        return $this->getData('base_original_price');
    }

    /**
     * @param float $baseOriginalPrice
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseOriginalPrice($baseOriginalPrice)
    {
        return $this->setData('base_original_price', $baseOriginalPrice);
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->getData('tax_percent');
    }

    /**
     * @param float $taxPercent
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxPercent($taxPercent)
    {
        return $this->setData('tax_percent', $taxPercent);
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->getData('tax_amount');
    }

    /**
     * @param float $taxAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxAmount($taxAmount)
    {
        return $this->setData('tax_amount', $taxAmount);
    }

    /**
     * @return float
     */
    public function getBaseTaxAmount()
    {
        return $this->getData('base_tax_amount');
    }

    /**
     * @param float $baseTaxAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxAmount($baseTaxAmount)
    {
        return $this->setData('base_tax_amount', $baseTaxAmount);
    }

    /**
     * @return float
     */
    public function getTaxInvoiced()
    {
        return $this->getData('tax_invoiced');
    }

    /**
     * @param float $taxInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxInvoiced($taxInvoiced)
    {
        return $this->setData('tax_invoiced', $taxInvoiced);
    }

    /**
     * @return float
     */
    public function getBaseTaxInvoiced()
    {
        return $this->getData('base_tax_invoiced');
    }

    /**
     * @param float $baseTaxInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxInvoiced($baseTaxInvoiced)
    {
        return $this->setData('base_tax_invoiced', $baseTaxInvoiced);
    }

    /**
     * @return float
     */
    public function getDiscountPercent()
    {
        return $this->getData('discount_percent');
    }

    /**
     * @param float $discountPercent
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountPercent($discountPercent)
    {
        return $this->setData('discount_percent', $discountPercent);
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->getData('discount_amount');
    }

    /**
     * @param float $discountAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountAmount($discountAmount)
    {
        return $this->setData('discount_amount', $discountAmount);
    }

    /**
     * @return float
     */
    public function getBaseDiscountAmount()
    {
        return $this->getData('base_discount_amount');
    }

    /**
     * @param float $baseDiscountAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountAmount($baseDiscountAmount)
    {
        return $this->setData('base_discount_amount', $baseDiscountAmount);
    }

    /**
     * @return float
     */
    public function getDiscountInvoiced()
    {
        return $this->getData('discount_invoiced');
    }

    /**
     * @param float $discountInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountInvoiced($discountInvoiced)
    {
        return $this->setData('discount_invoiced', $discountInvoiced);
    }

    /**
     * @return float
     */
    public function getBaseDiscountInvoiced()
    {
        return $this->getData('base_discount_invoiced');
    }

    /**
     * @param float $baseDiscountInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountInvoiced($baseDiscountInvoiced)
    {
        return $this->setData('base_discount_invoiced', $baseDiscountInvoiced);
    }

    /**
     * @return float
     */
    public function getAmountRefunded()
    {
        return $this->getData('amount_refunded');
    }

    /**
     * @param float $amountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setAmountRefunded($amountRefunded)
    {
        return $this->setData('amount_refunded', $amountRefunded);
    }

    /**
     * @return float
     */
    public function getBaseAmountRefunded()
    {
        return $this->getData('base_amount_refunded');
    }

    /**
     * @param float $baseAmountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseAmountRefunded($baseAmountRefunded)
    {
        return $this->setData('base_amount_refunded', $baseAmountRefunded);
    }

    /**
     * @return float
     */
    public function getRowTotal()
    {
        return $this->getData('row_total');
    }

    /**
     * @param float $rowTotal
     *
     * @return SalesOrderItemInterface
     */
    public function setRowTotal($rowTotal)
    {
        return $this->setData('row_total', $rowTotal);
    }

    /**
     * @return float
     */
    public function getBaseRowTotal()
    {
        return $this->getData('base_row_total');
    }

    /**
     * @param float $baseRowTotal
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowTotal($baseRowTotal)
    {
        return $this->setData('base_row_total', $baseRowTotal);
    }

    /**
     * @return float
     */
    public function getRowInvoiced()
    {
        return $this->getData('row_invoiced');
    }

    /**
     * @param float $rowInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setRowInvoiced($rowInvoiced)
    {
        return $this->setData('row_invoiced', $rowInvoiced);
    }

    /**
     * @return float
     */
    public function getBaseRowInvoiced()
    {
        return $this->getData('base_row_invoiced');
    }

    /**
     * @param float $baseRowInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowInvoiced($baseRowInvoiced)
    {
        return $this->setData('base_row_invoiced', $baseRowInvoiced);
    }

    /**
     * @return float
     */
    public function getRowWeight()
    {
        return $this->getData('row_weight');
    }

    /**
     * @param float $rowWeight
     *
     * @return SalesOrderItemInterface
     */
    public function setRowWeight($rowWeight)
    {
        return $this->setData('row_weight', $rowWeight);
    }

    /**
     * @return float
     */
    public function getBaseTaxBeforeDiscount()
    {
        return $this->getData('base_tax_before_discount');
    }

    /**
     * @param float $baseTaxBeforeDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxBeforeDiscount($baseTaxBeforeDiscount)
    {
        return $this->setData('base_tax_before_discount', $baseTaxBeforeDiscount);
    }

    /**
     * @return float
     */
    public function getTaxBeforeDiscount()
    {
        return $this->getData('tax_before_discount');
    }

    /**
     * @param float $taxBeforeDiscount
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxBeforeDiscount($taxBeforeDiscount)
    {
        return $this->setData('tax_before_discount', $taxBeforeDiscount);
    }

    /**
     * @return string
     */
    public function getExtOrderItemId()
    {
        return $this->getData('ext_order_item_id');
    }

    /**
     * @param string $extOrderItemId
     *
     * @return SalesOrderItemInterface
     */
    public function setExtOrderItemId($extOrderItemId)
    {
        return $this->setData('ext_order_item_id', $extOrderItemId);
    }

    /**
     * @return int
     */
    public function getLockedDoInvoice()
    {
        return $this->getData('locked_do_invoice');
    }

    /**
     * @param int $lockedDoInvoice
     *
     * @return SalesOrderItemInterface
     */
    public function setLockedDoInvoice($lockedDoInvoice)
    {
        return $this->setData('locked_do_invoice', $lockedDoInvoice);
    }

    /**
     * @return int
     */
    public function getLockedDoShip()
    {
        return $this->getData('locked_do_ship');
    }

    /**
     * @param int $lockedDoShip
     *
     * @return SalesOrderItemInterface
     */
    public function setLockedDoShip($lockedDoShip)
    {
        return $this->setData('locked_do_ship', $lockedDoShip);
    }

    /**
     * @return float
     */
    public function getPriceInclTax()
    {
        return $this->getData('price_incl_tax');
    }

    /**
     * @param float $priceInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setPriceInclTax($priceInclTax)
    {
        return $this->setData('price_incl_tax', $priceInclTax);
    }

    /**
     * @return float
     */
    public function getBasePriceInclTax()
    {
        return $this->getData('base_price_incl_tax');
    }

    /**
     * @param float $basePriceInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setBasePriceInclTax($basePriceInclTax)
    {
        return $this->setData('base_price_incl_tax', $basePriceInclTax);
    }

    /**
     * @return float
     */
    public function getRowTotalInclTax()
    {
        return $this->getData('row_total_incl_tax');
    }

    /**
     * @param float $rowTotalInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setRowTotalInclTax($rowTotalInclTax)
    {
        return $this->setData('row_total_incl_tax', $rowTotalInclTax);
    }

    /**
     * @return float
     */
    public function getBaseRowTotalInclTax()
    {
        return $this->getData('base_row_total_incl_tax');
    }

    /**
     * @param float $baseRowTotalInclTax
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseRowTotalInclTax($baseRowTotalInclTax)
    {
        return $this->setData('base_row_total_incl_tax', $baseRowTotalInclTax);
    }

    /**
     * @return float
     */
    public function getDiscountTaxCompensationAmount()
    {
        return $this->getData('discount_tax_compensation_amount');
    }

    /**
     * @param float $discountTaxCompensationAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationAmount($discountTaxCompensationAmount)
    {
        return $this->setData('discount_tax_compensation_amount', $discountTaxCompensationAmount);
    }

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationAmount()
    {
        return $this->getData('base_discount_tax_compensation_amount');
    }

    /**
     * @param float $baseDiscountTaxCompensationAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationAmount($baseDiscountTaxCompensationAmount)
    {
        return $this->setData('base_discount_tax_compensation_amount', $baseDiscountTaxCompensationAmount);
    }

    /**
     * @return float
     */
    public function getDiscountTaxCompensationInvoiced()
    {
        return $this->getData('discount_tax_compensation_invoiced');
    }

    /**
     * @param float $discountTaxCompensationInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationInvoiced($discountTaxCompensationInvoiced)
    {
        return $this->setData('discount_tax_compensation_invoiced', $discountTaxCompensationInvoiced);
    }

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationInvoiced()
    {
        return $this->getData('base_discount_tax_compensation_invoiced');
    }

    /**
     * @param float $baseDiscountTaxCompensationInvoiced
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationInvoiced($baseDiscountTaxCompensationInvoiced)
    {
        return $this->setData('base_discount_tax_compensation_invoiced', $baseDiscountTaxCompensationInvoiced);
    }

    /**
     * @return float
     */
    public function getDiscountTaxCompensationRefunded()
    {
        return $this->getData('discount_tax_compensation_refunded');
    }

    /**
     * @param float $discountTaxCompensationRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationRefunded($discountTaxCompensationRefunded)
    {
        return $this->setData('discount_tax_compensation_refunded', $discountTaxCompensationRefunded);
    }

    /**
     * @return float
     */
    public function getBaseDiscountTaxCompensationRefunded()
    {
        return $this->getData('base_discount_tax_compensation_refunded');
    }

    /**
     * @param float $baseDiscountTaxCompensationRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountTaxCompensationRefunded($baseDiscountTaxCompensationRefunded)
    {
        return $this->setData('base_discount_tax_compensation_refunded', $baseDiscountTaxCompensationRefunded);
    }

    /**
     * @return float
     */
    public function getTaxCanceled()
    {
        return $this->getData('tax_canceled');
    }

    /**
     * @param float $taxCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxCanceled($taxCanceled)
    {
        return $this->setData('tax_canceled', $taxCanceled);
    }

    /**
     * @return float
     */
    public function getDiscountTaxCompensationCanceled()
    {
        return $this->getData('discount_tax_compensation_canceled');
    }

    /**
     * @param float $discountTaxCompensationCanceled
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountTaxCompensationCanceled($discountTaxCompensationCanceled)
    {
        return $this->setData('discount_tax_compensation_canceled', $discountTaxCompensationCanceled);
    }

    /**
     * @return float
     */
    public function getTaxRefunded()
    {
        return $this->getData('tax_refunded');
    }

    /**
     * @param float $taxRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setTaxRefunded($taxRefunded)
    {
        return $this->setData('tax_refunded', $taxRefunded);
    }

    /**
     * @return float
     */
    public function getBaseTaxRefunded()
    {
        return $this->getData('base_tax_refunded');
    }

    /**
     * @param float $baseTaxRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseTaxRefunded($baseTaxRefunded)
    {
        return $this->setData('base_tax_refunded', $baseTaxRefunded);
    }

    /**
     * @return float
     */
    public function getDiscountRefunded()
    {
        return $this->getData('discount_refunded');
    }

    /**
     * @param float $discountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setDiscountRefunded($discountRefunded)
    {
        return $this->setData('discount_refunded', $discountRefunded);
    }

    /**
     * @return float
     */
    public function getBaseDiscountRefunded()
    {
        return $this->getData('base_discount_refunded');
    }

    /**
     * @param float $baseDiscountRefunded
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseDiscountRefunded($baseDiscountRefunded)
    {
        return $this->setData('base_discount_refunded', $baseDiscountRefunded);
    }

    /**
     * @return int
     */
    public function getFreeShipping()
    {
        return $this->getData('free_shipping');
    }

    /**
     * @param int $freeShipping
     *
     * @return SalesOrderItemInterface
     */
    public function setFreeShipping($freeShipping)
    {
        return $this->setData('free_shipping', $freeShipping);
    }

    /**
     * @return int
     */
    public function getGiftMessageId()
    {
        return $this->getData('gift_message_id');
    }

    /**
     * @param int $giftMessageId
     *
     * @return SalesOrderItemInterface
     */
    public function setGiftMessageId($giftMessageId)
    {
        return $this->setData('gift_message_id', $giftMessageId);
    }

    /**
     * @return int
     */
    public function getGiftMessageAvailable()
    {
        return $this->getData('gift_message_available');
    }

    /**
     * @param int $giftMessageAvailable
     *
     * @return SalesOrderItemInterface
     */
    public function setGiftMessageAvailable($giftMessageAvailable)
    {
        return $this->setData('gift_message_available', $giftMessageAvailable);
    }

    /**
     * @return string
     */
    public function getWeeeTaxApplied()
    {
        return $this->getData('weee_tax_applied');
    }

    /**
     * @param string $weeeTaxApplied
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxApplied($weeeTaxApplied)
    {
        return $this->setData('weee_tax_applied', $weeeTaxApplied);
    }

    /**
     * @return float
     */
    public function getWeeeTaxAppliedAmount()
    {
        return $this->getData('weee_tax_applied_amount');
    }

    /**
     * @param float $weeeTaxAppliedAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxAppliedAmount($weeeTaxAppliedAmount)
    {
        return $this->setData('weee_tax_applied_amount', $weeeTaxAppliedAmount);
    }

    /**
     * @return float
     */
    public function getWeeeTaxAppliedRowAmount()
    {
        return $this->getData('weee_tax_applied_row_amount');
    }

    /**
     * @param float $weeeTaxAppliedRowAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxAppliedRowAmount($weeeTaxAppliedRowAmount)
    {
        return $this->setData('weee_tax_applied_row_amount', $weeeTaxAppliedRowAmount);
    }

    /**
     * @return float
     */
    public function getWeeeTaxDisposition()
    {
        return $this->getData('weee_tax_disposition');
    }

    /**
     * @param float $weeeTaxDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxDisposition($weeeTaxDisposition)
    {
        return $this->setData('weee_tax_disposition', $weeeTaxDisposition);
    }

    /**
     * @return float
     */
    public function getWeeeTaxRowDisposition()
    {
        return $this->getData('weee_tax_row_disposition');
    }

    /**
     * @param float $weeeTaxRowDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setWeeeTaxRowDisposition($weeeTaxRowDisposition)
    {
        return $this->setData('weee_tax_row_disposition', $weeeTaxRowDisposition);
    }

    /**
     * @return float
     */
    public function getBaseWeeeTaxAppliedAmount()
    {
        return $this->getData('base_weee_tax_applied_amount');
    }

    /**
     * @param float $baseWeeeTaxAppliedAmount
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxAppliedAmount($baseWeeeTaxAppliedAmount)
    {
        return $this->setData('base_weee_tax_applied_amount', $baseWeeeTaxAppliedAmount);
    }

    /**
     * @return float
     */
    public function getBaseWeeeTaxAppliedRowAmnt()
    {
        return $this->getData('base_weee_tax_applied_row_amnt');
    }

    /**
     * @param float $baseWeeeTaxAppliedRowAmnt
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxAppliedRowAmnt($baseWeeeTaxAppliedRowAmnt)
    {
        return $this->setData('base_weee_tax_applied_row_amnt', $baseWeeeTaxAppliedRowAmnt);
    }

    /**
     * @return float
     */
    public function getBaseWeeeTaxDisposition()
    {
        return $this->getData('base_weee_tax_disposition');
    }

    /**
     * @param float $baseWeeeTaxDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxDisposition($baseWeeeTaxDisposition)
    {
        return $this->setData('base_weee_tax_disposition', $baseWeeeTaxDisposition);
    }

    /**
     * @return float
     */
    public function getBaseWeeeTaxRowDisposition()
    {
        return $this->getData('base_weee_tax_row_disposition');
    }

    /**
     * @param float $baseWeeeTaxRowDisposition
     *
     * @return SalesOrderItemInterface
     */
    public function setBaseWeeeTaxRowDisposition($baseWeeeTaxRowDisposition)
    {
        return $this->setData('base_weee_tax_row_disposition', $baseWeeeTaxRowDisposition);
    }

}
