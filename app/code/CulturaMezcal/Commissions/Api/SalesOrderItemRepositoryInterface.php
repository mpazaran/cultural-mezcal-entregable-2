<?php

namespace CulturaMezcal\Commissions\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @package CulturaMezcal\Commissions\Api
 */
interface SalesOrderItemRepositoryInterface extends OptionSourceInterface
{

    /**
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     */
    public function create();

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return \CulturaMezcal\Commissions\Api\SalesOrderItemSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @return \CulturaMezcal\Commissions\Model\ResourceModel\SalesOrderItem\Collection
     */
    public function getCollection();

    /**
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function load($value, $field = null);

    /**
     * @param \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model
     * @param mixed  $value
     * @param string $field
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function loadModel(\CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model, $value, $field = null);

    /**
     * @param \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     */
    public function save(\CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model);

    /**
     * @param \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     */
    public function delete(\CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface $model);

    /**
     * @param int $id
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface
     * @throws NoSuchEntityException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return array
     */
    public function toOptionArray(SearchCriteriaInterface $searchCriteria = null);

}
