<?php

namespace CulturaMezcal\Commissions\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface SalesOrderItemSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get customer groups list.
     *
     * @return \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface[]
     */
    public function getItems();

    /**
     * Set customer groups list.
     *
     * @api
     * @param \CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
