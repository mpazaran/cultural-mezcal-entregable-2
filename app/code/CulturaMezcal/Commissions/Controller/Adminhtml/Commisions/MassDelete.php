<?php

namespace CulturaMezcal\Commissions\Controller\Adminhtml\Commisions;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * MassDelete constructor.
     *
     * @param Action\Context $context
     * @param Logger         $logger
     */
    public function __construct(
        Action\Context $context,
        Logger $logger
    ) {
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        //throw new \Exception('Not implemented');

        $itemIds = $this->getRequest()->getParam('selected');
        if (!is_array($itemIds) || empty($itemIds)) {
            $this->messageManager->addErrorMessage(__('Please select item(s).'));
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $post = $this->_objectManager->get('CulturaMezcal\Commissions\Api\Data\SalesOrderItemInterface')->load($itemId);
                    $post->delete();
                }
                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been deleted.', count($itemIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Product'));
            }
        }

        return $this->resultRedirectFactory->create()->setPath('forwards/*/index');
    }
}
