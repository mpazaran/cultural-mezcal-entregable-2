<?php

namespace CulturaMezcal\Commissions\Controller\Adminhtml\Commisions;

use CulturaMezcal\Commissions\Api\SalesOrderItemRepositoryInterface as RepositoryInterface;
use CulturaMezcal\Commissions\Logger\Logger;
use Magento\Backend\App\Action;
USE Magento\Catalog\Model\ImageUploader;

class Save extends Action
{
   /**
    * @var CulturaMezcal\Commissions\Api\SalesOrderItemRepositoryInterface
    */
   protected $repository;
    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @param Action\Context      $context
     * @param RepositoryInterface $repository
     * @param Logger              $logger
     */
    public function __construct(
        Action\Context $context,
        RepositoryInterface $repository,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->repository    = $repository;
        $this->logger        = $logger;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $model = $this->repository->create('id');

            $id = $this->getRequest()->getParam('id');

            if ($id) {
                $this->repository->loadModel($model, $id);
            }

            try {

                $model
                                ->setOrderId($data['order_id'] ?? null)
                ->setParentItemId($data['parent_item_id'] ?? null)
                ->setQuoteItemId($data['quote_item_id'] ?? null)
                ->setStoreId($data['store_id'] ?? null)
                ->setProductId($data['product_id'] ?? null)
                ->setProductType($data['product_type'] ?? null)
                ->setProductOptions($data['product_options'] ?? null)
                ->setWeight($data['weight'] ?? null)
                ->setIsVirtual($data['is_virtual'] ?? null)
                ->setSku($data['sku'] ?? null)
                ->setName($data['name'] ?? null)
                ->setDescription($data['description'] ?? null)
                ->setAppliedRuleIds($data['applied_rule_ids'] ?? null)
                ->setAdditionalData($data['additional_data'] ?? null)
                ->setIsQtyDecimal($data['is_qty_decimal'] ?? null)
                ->setNoDiscount($data['no_discount'] ?? null)
                ->setQtyBackordered($data['qty_backordered'] ?? null)
                ->setQtyCanceled($data['qty_canceled'] ?? null)
                ->setQtyInvoiced($data['qty_invoiced'] ?? null)
                ->setQtyOrdered($data['qty_ordered'] ?? null)
                ->setQtyRefunded($data['qty_refunded'] ?? null)
                ->setQtyShipped($data['qty_shipped'] ?? null)
                ->setBaseCost($data['base_cost'] ?? null)
                ->setPrice($data['price'] ?? null)
                ->setBasePrice($data['base_price'] ?? null)
                ->setOriginalPrice($data['original_price'] ?? null)
                ->setBaseOriginalPrice($data['base_original_price'] ?? null)
                ->setTaxPercent($data['tax_percent'] ?? null)
                ->setTaxAmount($data['tax_amount'] ?? null)
                ->setBaseTaxAmount($data['base_tax_amount'] ?? null)
                ->setTaxInvoiced($data['tax_invoiced'] ?? null)
                ->setBaseTaxInvoiced($data['base_tax_invoiced'] ?? null)
                ->setDiscountPercent($data['discount_percent'] ?? null)
                ->setDiscountAmount($data['discount_amount'] ?? null)
                ->setBaseDiscountAmount($data['base_discount_amount'] ?? null)
                ->setDiscountInvoiced($data['discount_invoiced'] ?? null)
                ->setBaseDiscountInvoiced($data['base_discount_invoiced'] ?? null)
                ->setAmountRefunded($data['amount_refunded'] ?? null)
                ->setBaseAmountRefunded($data['base_amount_refunded'] ?? null)
                ->setRowTotal($data['row_total'] ?? null)
                ->setBaseRowTotal($data['base_row_total'] ?? null)
                ->setRowInvoiced($data['row_invoiced'] ?? null)
                ->setBaseRowInvoiced($data['base_row_invoiced'] ?? null)
                ->setRowWeight($data['row_weight'] ?? null)
                ->setBaseTaxBeforeDiscount($data['base_tax_before_discount'] ?? null)
                ->setTaxBeforeDiscount($data['tax_before_discount'] ?? null)
                ->setExtOrderItemId($data['ext_order_item_id'] ?? null)
                ->setLockedDoInvoice($data['locked_do_invoice'] ?? null)
                ->setLockedDoShip($data['locked_do_ship'] ?? null)
                ->setPriceInclTax($data['price_incl_tax'] ?? null)
                ->setBasePriceInclTax($data['base_price_incl_tax'] ?? null)
                ->setRowTotalInclTax($data['row_total_incl_tax'] ?? null)
                ->setBaseRowTotalInclTax($data['base_row_total_incl_tax'] ?? null)
                ->setDiscountTaxCompensationAmount($data['discount_tax_compensation_amount'] ?? null)
                ->setBaseDiscountTaxCompensationAmount($data['base_discount_tax_compensation_amount'] ?? null)
                ->setDiscountTaxCompensationInvoiced($data['discount_tax_compensation_invoiced'] ?? null)
                ->setBaseDiscountTaxCompensationInvoiced($data['base_discount_tax_compensation_invoiced'] ?? null)
                ->setDiscountTaxCompensationRefunded($data['discount_tax_compensation_refunded'] ?? null)
                ->setBaseDiscountTaxCompensationRefunded($data['base_discount_tax_compensation_refunded'] ?? null)
                ->setTaxCanceled($data['tax_canceled'] ?? null)
                ->setDiscountTaxCompensationCanceled($data['discount_tax_compensation_canceled'] ?? null)
                ->setTaxRefunded($data['tax_refunded'] ?? null)
                ->setBaseTaxRefunded($data['base_tax_refunded'] ?? null)
                ->setDiscountRefunded($data['discount_refunded'] ?? null)
                ->setBaseDiscountRefunded($data['base_discount_refunded'] ?? null)
                ->setFreeShipping($data['free_shipping'] ?? null)
                ->setGiftMessageId($data['gift_message_id'] ?? null)
                ->setGiftMessageAvailable($data['gift_message_available'] ?? null)
                ->setWeeeTaxApplied($data['weee_tax_applied'] ?? null)
                ->setWeeeTaxAppliedAmount($data['weee_tax_applied_amount'] ?? null)
                ->setWeeeTaxAppliedRowAmount($data['weee_tax_applied_row_amount'] ?? null)
                ->setWeeeTaxDisposition($data['weee_tax_disposition'] ?? null)
                ->setWeeeTaxRowDisposition($data['weee_tax_row_disposition'] ?? null)
                ->setBaseWeeeTaxAppliedAmount($data['base_weee_tax_applied_amount'] ?? null)
                ->setBaseWeeeTaxAppliedRowAmnt($data['base_weee_tax_applied_row_amnt'] ?? null)
                ->setBaseWeeeTaxDisposition($data['base_weee_tax_disposition'] ?? null)
                ->setBaseWeeeTaxRowDisposition($data['base_weee_tax_row_disposition'] ?? null);
                $this->repository->save($model);

                if($model->getId()){
                    $this->messageManager->addSuccessMessage(__('Product has been saved.'));
                    $this->_session->setData('culturamezcal_commissions_commisions_form_data', false);

                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                    }

                    return $resultRedirect->setPath('*/*/');

                }

                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving %1.', 'Product'));

                $this->_session->setData('culturamezcal_commissions_commisions_form_data', $data);

                return $resultRedirect->setPath('*/*/new');

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Product'));
            } catch (\RuntimeException $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Product'));
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while delete %1.', 'Product'));
            }

            $this->_session->setData('culturamezcal_commissions_commisions_form_data', $data);

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
