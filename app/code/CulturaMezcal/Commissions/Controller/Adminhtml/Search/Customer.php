<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace CulturaMezcal\Commissions\Controller\Adminhtml\Search;

use Magento\Backend\App\Action\Context;use Magento\Customer\Api\CustomerRepositoryInterface as RepositoryInterface;use Magento\Customer\Api\Data\CustomerInterface as DataInterface;use Magento\Framework\Api\FilterBuilder;use Magento\Framework\Api\Search\FilterGroupBuilder;use Magento\Framework\Api\SearchCriteriaBuilder;use Magento\Framework\Api\SearchResults;use Magento\Framework\Api\SortOrderBuilder;use Magento\Framework\App\Action\HttpGetActionInterface;use Magento\Framework\Controller\Result\Json;use Magento\Framework\Controller\Result\JsonFactory;use Magento\Framework\Controller\ResultInterface;

/**
 * Controller to search product for ui-select component
 */
class Customer extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'CulturaMezcal_Commissions::sales_order_item_write';

    /**
     * @var JsonFactory
     */

    private $resultJsonFactory;
    /**
     * @var RepositoryInterface
     */

    private $repository;
    /**
     * @var SearchCriteriaBuilder
     */

    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */

    private $filterBuilder;
    /**
     * @var FilterGroupBuilder
     */

    private $filterGroupBuilder;
    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param JsonFactory           $resultFactory
     * @param RepositoryInterface   $repository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder         $filterBuilder
     * @param FilterGroupBuilder    $filterGroupBuilder
     * @param SortOrderBuilder      $sortOrderBuilder
     * @param Context               $context
     */
    public function __construct(
        JsonFactory $resultFactory,
        RepositoryInterface $repository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder,
        Context $context
    ) {
        $this->resultJsonFactory = $resultFactory;

        parent::__construct($context);
        $this->repository            = $repository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroupBuilder    = $filterGroupBuilder;
        $this->sortOrderBuilder      = $sortOrderBuilder;
    }

    /**
     * Execute customer search.
     *
     * @return ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(): ResultInterface
    {

        $id        = $this->getRequest()->getParam('id');
        $searchKey = $this->getRequest()->getParam('searchKey');
        $pageNum   = (int)$this->getRequest()->getParam('page') ?? 1;
        $limit     = (int)$this->getRequest()->getParam('limit') ?? 50;

        $this->searchCriteriaBuilder
            ->setCurrentPage($pageNum)
            ->setPageSize($limit);

        if(!empty($id)){
            $this->searchCriteriaBuilder->addFilter('entity_id', $id);
        } else {
            if (!empty($searchKey)) {
                $this->filterGroupBuilder->addFilter(
                    $this->filterBuilder->setField('firstname')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                )->addFilter(
                    $this->filterBuilder->setField('lastname')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                )->addFilter(
                    $this->filterBuilder->setField('email')
                        ->setValue('%' . $searchKey . '%')
                        ->setConditionType('like')
                        ->create()
                );
                $this->searchCriteriaBuilder->setFilterGroups([$this->filterGroupBuilder->create()]);
            }
        }

        $this->searchCriteriaBuilder->setSortOrders([
            $this->sortOrderBuilder->setField('firstname')->create(),
            $this->sortOrderBuilder->setField('lastname')->create(),
            $this->sortOrderBuilder->setField('email')->create()
        ]);

        /** @var SearchResults $itemList */
        $itemList    = $this->repository->getList($this->searchCriteriaBuilder->create());
        $totalValues = $itemList->getTotalCount();
        $items       = [];
        /** @var DataInterface $item */
        foreach ($itemList->getItems() as $item) {
            $itemId         = $item->getId();
            $items[$itemId] = [
                'value'     => $itemId,
                'label'     => $item->getFirstname() . ' ' . $item->getLastname() . ' <' . $item->getEmail() . '>',
                'is_active' => $item->getConfirmation() ?? 1,
                'path'      => $item->getEmail(),
                'optgroup'  => false
            ];
        }
        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
            'options' => $items,
            'total'   => empty($items) ? 0 : $totalValues
        ]);
    }
}
