define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'directdebit',
                component: 'CulturaMezcal_DirectDebit/js/view/payment/method-renderer/directdebit'
            }
        );
        return Component.extend({});
    }
);
