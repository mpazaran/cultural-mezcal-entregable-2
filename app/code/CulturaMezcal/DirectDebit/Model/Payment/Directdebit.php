<?php

/** @noinspection PhpDeprecationInspection */

namespace CulturaMezcal\DirectDebit\Model\Payment;

use CulturaMezcal\Core\Helper\Data;
use CulturaMezcal\DirectDebit\Block\Payment\Directdebit\Form;
use CulturaMezcal\DirectDebit\Block\Payment\Directdebit\Info;
use Exception;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;

class Directdebit extends AbstractMethod
{
    const CODE = 'directdebit';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var string
     */
    protected $_formBlockType = Form::class;

    /**
     * @var string
     */
    protected $_infoBlockType = Info::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * @var State
     */
    protected $state;
    /**
     * @var Data
     */
    private $coreHelper;

    /**
     * @param Context                      $context
     * @param Registry                     $registry
     * @param ExtensionAttributesFactory   $extensionFactory
     * @param AttributeValueFactory        $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param ScopeConfigInterface         $scopeConfig
     * @param Logger                       $logger
     * @param State                        $state
     * @param Data                         $coreHelper
     * @param AbstractResource|null        $resource
     * @param AbstractDb|null              $resourceCollection
     * @param array                        $data
     * @param DirectoryHelper|null         $directory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        State $state,
        Data $coreHelper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );
        $this->state      = $state;
        $this->coreHelper = $coreHelper;
    }

    public function isActive($storeId = null)
    {
        if ((bool)$this->getConfigData('active', $storeId)) {
            try {
                $customerGroup = $this->coreHelper->getCustomer()->getGroupId();
            } catch (Exception $e) {
                return false;
            }

            return $this->getConfigData('forward_customer_group') == $customerGroup || $this->getConfigData('horeca_customer_group') == $customerGroup;
        }

        return false;
    }
}
