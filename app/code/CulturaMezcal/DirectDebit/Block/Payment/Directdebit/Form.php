<?php

namespace CulturaMezcal\DirectDebit\Block\Payment\Directdebit;

class Form extends \Magento\Payment\Block\Form
{
    /**
     * Checkmo template
     *
     * @var string
     */
    protected $_template = 'CulturaMezcal_DirectDebit::payment/directdebit/form.phtml';
}
