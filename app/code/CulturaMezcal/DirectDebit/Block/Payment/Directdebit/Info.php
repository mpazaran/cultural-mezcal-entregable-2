<?php

namespace CulturaMezcal\DirectDebit\Block\Payment\Directdebit;

class Info extends \Magento\Payment\Block\Info
{

    /**
     * @var string
     */
    protected $_template = 'CulturaMezcal_DirectDebit::payment/directdebit/info.phtml';

}
