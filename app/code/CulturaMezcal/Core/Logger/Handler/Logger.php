<?php

namespace CulturaMezcal\Core\Logger\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger as MonoLogger;

class Logger extends Base
{

    /**
     * @var string
     */
    protected $fileName = '/var/log/cultura_mezcal_core.log';

    /**
     * @var
     */
    protected $loggerType = MonoLogger::DEBUG;

}
