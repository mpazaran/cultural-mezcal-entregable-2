<?php

namespace CulturaMezcal\Core\Model;

use Magento\Eav\Model\Config;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class to get cms blocks as option array
 */
class AttributeOptions implements OptionSourceInterface
{
    /**
     * @var Config
     */
    private $eavConfig;

    private $attributeCode;

    /**
     * AttributeOptions constructor.
     *
     * @param Config $eavConfig
     * @param null   $attributeCode
     */
    public function __construct(
        Config $eavConfig,
        $attributeCode = null
    ) {
        $this->eavConfig     = $eavConfig;
        $this->attributeCode = $attributeCode;
    }

    /**
     * @param string $type
     *
     * @return array
     * @throws LocalizedException
     */
    public function toOptionArray(string $type = 'customer'): array
    {
        $attribute = $this->eavConfig->getAttribute($type, $this->attributeCode);

        return $attribute->getSource()->getAllOptions();
    }

    /**
     * @param mixed $attributeCode
     *
     * @return AttributeOptions
     */
    public function setAttributeCode($attributeCode)
    {
        $this->attributeCode = $attributeCode;

        return $this;
    }
}
